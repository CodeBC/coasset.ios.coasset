//
//  Deal.h
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Deal : NSManagedObject

@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSNumber * interested;
@property (nonatomic, retain) NSDecimalNumber * max_annual_return;
@property (nonatomic, retain) NSDecimalNumber * min_annual_return;
@property (nonatomic, retain) NSNumber * min_investment;
@property (nonatomic, retain) NSString * offer_title;
@property (nonatomic, retain) NSString * offer_type;
@property (nonatomic, retain) NSString * owner_type;
@property (nonatomic, retain) NSString * short_description;
@property (nonatomic, retain) NSNumber * time_horizon;
@property (nonatomic, retain) NSString * highlight;
@property (nonatomic, retain) NSString * long_description;
@property (nonatomic, retain) NSNumber * tokens_per_fancy;

@end
