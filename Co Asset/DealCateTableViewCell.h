//
//  DealCateTableViewCell.h
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDealCate.h"
@interface DealCateTableViewCell : UITableViewCell
- (void)setUpViews;
- (void)viewLoadWithDealCate:(ObjDealCate *)obj;
@end
