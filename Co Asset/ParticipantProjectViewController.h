//
//  ParticipantProjectViewController.h
//  Co Asset
//
//  Created by Zayar on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjUser.h"
@interface ParticipantProjectViewController : BasedViewController
@property NSInteger is_from;
@property (nonatomic, strong) ObjUser * objUser;
@end
