//
//  DealCateTableViewCell.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DealOngoingTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
@interface DealOngoingTableViewCell()
@property (nonatomic,strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIView * titleBgView;
@property (nonatomic, strong) UILabel * lblName;
@property (nonatomic, strong) UILabel * lblInvestorIntreset;
@property (nonatomic, strong) UILabel * lblDayLeft;
@property (nonatomic, strong) UIButton * btnSearch;
@property (nonatomic, strong) UIButton * btnFav;
@end
@implementation DealOngoingTableViewCell
- (UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, DEAL_CELL_HEIGHT)];
        _bgImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _bgImageView;
}

- (UIView *) titleBgView{
    if (!_titleBgView) {
        _titleBgView = [[UIView alloc] initWithFrame:CGRectMake(0, DEAL_CELL_HEIGHT-60, 320, 60)];
        [_titleBgView setBackgroundColor:[UIColor darkTextColor]];
        [_titleBgView setAlpha:0.4];
    }
    return _titleBgView;
}

- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(10, DEAL_CELL_HEIGHT-60,300 , 30)];//
        [_lblName setBackgroundColor:[UIColor clearColor]];
        _lblName.textColor = [UIColor whiteColor];
        _lblName.font = [UIFont boldSystemFontOfSize:14];
    }
    return _lblName;
}

- (UILabel *) lblInvestorIntreset{
    if (!_lblInvestorIntreset) {
        _lblInvestorIntreset = [[UILabel alloc] initWithFrame:CGRectMake(10, DEAL_CELL_HEIGHT-40, 320-20, 40)];
        [_lblInvestorIntreset setBackgroundColor:[UIColor clearColor]];
        _lblInvestorIntreset.textColor = [UIColor whiteColor];
        _lblInvestorIntreset.font = [UIFont systemFontOfSize:11];
        _lblInvestorIntreset.numberOfLines = 2;
    }
    return _lblInvestorIntreset;
}

/*- (UILabel *) lblDayLeft{
    if (!_lblDayLeft) {
        _lblDayLeft = [[UILabel alloc] initWithFrame:CGRectMake(self.lblInvestorIntreset.frame.origin.x + self.lblInvestorIntreset.frame.size.width + 10, DEAL_CELL_HEIGHT-35, 320-200, 30)];
        [_lblDayLeft setBackgroundColor:[UIColor clearColor]];
        _lblDayLeft.textColor = [UIColor whiteColor];
        _lblDayLeft.font = [UIFont systemFontOfSize:11];
    }
    return _lblDayLeft;
}*/

-(UIButton *)btnFav{
    if (!_btnFav) {
        _btnFav = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnFav setFrame:CGRectMake(self.titleBgView.frame.origin.x + self.titleBgView.frame.size.width - 40, DEAL_CELL_HEIGHT-50, 30, 30)];
        [_btnFav setBackgroundColor:[UIColor clearColor]];
        [_btnFav addTarget:self action:@selector(onFav:) forControlEvents:UIControlEventTouchUpInside];
        [_btnFav setImage:[UIImage imageNamed:@"heart_icon"] forState:UIControlStateNormal];
    }
    return _btnFav;
}

- (void)setUpViews{
    [self addSubview:self.bgImageView];
    [self addSubview:self.titleBgView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblInvestorIntreset];
    [self addSubview:self.lblDayLeft];
    //[self addSubview:self.btnFav];
}

- (void) onFav:(UIButton *)sender{
    
}

- (void)viewLoadWithDealOngoing:(ObjDeal *)obj{
    //NSLog(@"Ongoing image link %@",[NSString stringWithFormat:@"%@%@",APP_BASED_LINK,obj.project.photo]);
    [self.bgImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",obj.project.photo]] placeholderImage:[UIImage imageNamed:@"ongoing_deal_bg"]];
    [self.lblName setText:obj.deal.offer_title];
    //[self setInvestorIntreset:@"Interseted Investors" andName:[NSString stringWithFormat:@"%d",[obj.deal.interested integerValue]]];
    //[self setDayLeft:@"Min Investment" andName:[NSString stringWithFormat:@"%d",[obj.deal.min_investment integerValue]]];
    [self setInvestor:[obj.deal.interested integerValue] andMinInvestment:[obj.deal.min_investment integerValue] andReturn:[obj.deal.min_annual_return integerValue] andCurrencyCode:obj.deal.currency];
}

- (void) setInvestor:(NSInteger)investor andMinInvestment:(NSInteger)minInvestment andReturn:(NSInteger) annual_return andCurrencyCode:(NSString *)strCurrencyCode{
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [_currencyFormatter setCurrencyCode:strCurrencyCode];
    [self.lblInvestorIntreset setText:[NSString stringWithFormat:@"Interested Investors : %d   Min Investment : %@ \nAnnual Return : %d%%",investor,[_currencyFormatter stringFromNumber:[NSNumber numberWithInteger:minInvestment]],annual_return]];
}

- (void) setInvestorIntreset:(NSString *)strCapName andName:(NSString *)strName{
    self.lblInvestorIntreset.text = [NSString stringWithFormat:@"%@ : %@",strCapName,strName];
}

- (void) setDayLeft:(NSString *)strCapName andName:(NSString *)strName{
    self.lblDayLeft.text = [NSString stringWithFormat:@"%@ %@",strCapName,strName];
}

@end
