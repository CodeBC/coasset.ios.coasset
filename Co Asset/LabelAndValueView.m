//
//  ProjectTypeView.m
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LabelAndValueView.h"
@interface LabelAndValueView()
@property (nonatomic,strong) UILabel * lblCap;
@property (nonatomic,strong) UILabel * lblName;
@end
@implementation LabelAndValueView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (UILabel *) lblCap{
    if (!_lblCap) {
        _lblCap = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 20, 30)];
        _lblCap.backgroundColor = [UIColor clearColor];
        _lblCap.textColor = [UIColor blackColor];
        _lblCap.font = [UIFont systemFontOfSize:14];
    }
    return _lblCap;
}

- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.lblCap.frame.origin.x + self.lblCap.frame.size.width+3, 40)];
        _lblName.backgroundColor = [UIColor clearColor];
        _lblName.textColor = [UIColor blackColor];
        _lblName.font = [UIFont systemFontOfSize:14];
    }
    return _lblName;
}

- (void)setUpViews{
    [self addSubview:self.lblCap];
    //[self addSubview:self.lblName];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) loadWithCapName:(NSString *)strCapName andName:(NSString *)strName{
    self.lblCap.text = [NSString stringWithFormat:@"%@ : %@",strCapName,strName];
}

@end
