//
//  NewsCollectionViewCell.m
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NewsCollectionViewCell.h"

@implementation NewsCollectionViewCell
{
    UILabel * lblTitle;
    UILabel * lblTime;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setupCell{
    if (!lblTitle){
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 20)] ;
        
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        [self addSubview:lblTitle];
    }
    if (!lblTime){
        lblTime = [[UILabel alloc] initWithFrame:CGRectMake(20, 45, 200, 20)] ;
        
        lblTime.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11];
        [self addSubview:lblTime];
    }
}

- (void) loadCell{
    
}

- (void) loadCellWithNews:(ObjNews *)obj{
    lblTitle.text = obj.news.title;
    lblTime.text = [obj getHumanDate];
}

@end
