//
//  NewsDetailViewController.h
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjNews.h"
@interface NewsDetailViewController : BasedViewController <UIScrollViewDelegate>
@property (nonatomic, strong) ObjNews * objNews;

@end
