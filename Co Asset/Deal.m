//
//  Deal.m
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Deal.h"


@implementation Deal

@dynamic currency;
@dynamic idx;
@dynamic interested;
@dynamic max_annual_return;
@dynamic min_annual_return;
@dynamic min_investment;
@dynamic offer_title;
@dynamic offer_type;
@dynamic owner_type;
@dynamic short_description;
@dynamic time_horizon;
@dynamic highlight;
@dynamic long_description;
@dynamic tokens_per_fancy;

@end
