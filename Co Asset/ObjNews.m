//
//  ObjNews.m
//  Co Asset
//
//  Created by Zayar on 5/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ObjNews.h"
#import "Utility.h"
/*
 "id": 1194,
 "title": "iProperty To Organise Second Bumiputera Property Expo",
 "author": "admin",
 "date_added": "2014-05-06T20:05:05Z",
 "section_name": "Malaysia News"
 */
@implementation ObjNews
@synthesize news;
+ (ObjNews*)sharedClient{
    static ObjNews *_sharedClient = nil;
    
    _sharedClient = [[ObjNews alloc] init];
    
    if(_sharedClient) {
        //_sharedClient = [[DBManager sharedClient] getConfig];
    }
    return _sharedClient;
}

+ (ObjNews *) setNewsSaveAndUpdateFromDic:(NSDictionary *)dic{
    ObjNews * objNews = nil;
    if ([dic count]) {
        objNews = [[ObjNews alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * title = [dic objectForKey:@"title"];
        NSString * author = [dic objectForKey:@"author"];
        NSString * strDate = [dic objectForKey:@"date_added"];
        NSDate * date = nil;
        NSString * section_name = [dic objectForKey:@"section_name"];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:title shouldCleanWhiteSpace:YES]) title = @"";
        if ([Utility stringIsEmpty:author shouldCleanWhiteSpace:YES]) author = @"";
        if ([Utility stringIsEmpty:strDate shouldCleanWhiteSpace:YES])
            strDate = @"";
        else{
            date = [Utility dateFromInternetDateTimeString:strDate];
            NSLog(@"strDate %@ and date: date %@",strDate,date);
        }
        if ([Utility stringIsEmpty:section_name shouldCleanWhiteSpace:YES]) section_name = @"";
        
        
        News * obj = [News MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.title = title;
            obj.author = author;
            obj.date = date;
            obj.section_name = section_name;
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
            objNews.news = obj;
        }
        else{
            News * obj2 = [News MR_createEntity];
            obj2.idx = strUniId;
            obj2.title = title;
            obj2.author = author;
            obj2.date = date;
            obj2.section_name = section_name;
            
            [obj2 MR_inContext:localContext];
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"News saved");
            }];
            objNews.news = obj2;
        }
    }
    return objNews;
}

+ (ObjNews *) setNewsDetailSaveAndUpdateFromDic:(NSDictionary *)dic{
    ObjNews * obj = nil;
    if ([dic count]) {
        obj = [[ObjNews alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * title = [dic objectForKey:@"title"];
        NSString * strDate = [dic objectForKey:@"date_added"];
        NSString * text_content = [dic objectForKey:@"text_content"];
        NSString * graphic_content = [dic objectForKey:@"graphic_content"];
        NSDate * date = nil;
        NSString * section_name = [dic objectForKey:@"section_name"];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:title shouldCleanWhiteSpace:YES]) title = @"";
        if ([Utility stringIsEmpty:strDate shouldCleanWhiteSpace:YES])
            strDate = @"";
        else{
            date = [Utility dateFromInternetDateTimeString:strDate];
            NSLog(@"strDate %@ and date: date %@",strDate,date);
        }
        if ([Utility stringIsEmpty:section_name shouldCleanWhiteSpace:YES]) section_name = @"";
        if ([Utility stringIsEmpty:text_content shouldCleanWhiteSpace:YES])
            text_content = @"";
        if ([Utility stringIsEmpty:graphic_content shouldCleanWhiteSpace:YES])
            graphic_content = @"";
        
        
        News * obj = [News MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.title = title;
            obj.date = date;
            obj.section_name = section_name;
            obj.detail_content = text_content;
            obj.graphic_content = graphic_content;
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
        }
        else{
            News * obj2 = [News MR_createEntity];
            obj2.idx = strUniId;
            obj2.title = title;
            obj2.date = date;
            obj2.section_name = section_name;
            obj2.detail_content = text_content;
            obj2.graphic_content = graphic_content;
            [obj2 MR_inContext:localContext];
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"News Detail saved");
            }];
        }
    }
    return obj;
}

+ (NSMutableArray *) getNewsArray{
    NSMutableArray * arr=nil;
    arr = (NSMutableArray *)[News MR_findAll];
    NSMutableArray * arrNews = nil;
    if ([arr count]) {
        arrNews = [[NSMutableArray alloc] init];
    }
    
    for (News * obj in arr) {
        ObjNews * objNews = [[ObjNews alloc] init];
        objNews.news = obj;
        [arrNews addObject:objNews];
    }
    return arrNews;
}

- (NSString *) getHumanDate{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"SGT"]];
    [dateFormatter setDateFormat:@"MMM dd yyyy, HH:mm a"];
    NSString * str = @"";
    str = [dateFormatter stringFromDate:self.news.date];
    NSLog(@"human date %@",self.news.date);
    return str;
}

+ (NSMutableArray *) getParseArray:(NSArray *)dicArrNews{
    ObjNews * objNews = nil;
    NSMutableArray * arrNews =nil;
    if ([dicArrNews count]) {
        arrNews = [[NSMutableArray alloc] init];
    }
    for(NSInteger i=0;i<[dicArrNews count];i++){
        objNews = [ObjNews parseNews:[dicArrNews objectAtIndex:i]];
        [arrNews addObject:objNews];
    }
    return arrNews;
}

+ (ObjNews *) parseNews:(NSDictionary *)dic{
    ObjNews * objNews = nil;
    if ([dic count]) {
        objNews = [[ObjNews alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * title = [dic objectForKey:@"title"];
        NSString * author = [dic objectForKey:@"author"];
        NSString * strDate = [dic objectForKey:@"date_added"];
        NSDate * date = nil;
        NSString * section_name = [dic objectForKey:@"section_name"];
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:title shouldCleanWhiteSpace:YES]) title = @"";
        if ([Utility stringIsEmpty:author shouldCleanWhiteSpace:YES]) author = @"";
        if ([Utility stringIsEmpty:strDate shouldCleanWhiteSpace:YES])
            strDate = @"";
        else{
            date = [Utility dateFromInternetDateTimeString:strDate];
            NSLog(@"strDate %@ and date: date %@",strDate,date);
        }
        if ([Utility stringIsEmpty:section_name shouldCleanWhiteSpace:YES]) section_name = @"";
        
        News * obj2 = [News MR_createEntity];
        obj2.idx = strUniId;
        obj2.title = title;
        obj2.author = author;
        obj2.date = date;
        obj2.section_name = section_name;
        objNews.news = obj2;
    }
    return  objNews;
}


@end
