//
//  OngoingDealDetailViewController.h
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDeal.h"
#import "Deal.h"
@interface OngoingDealDetailViewController : BasedViewController
@property (nonatomic,strong) ObjDeal * objDeal;
@property (nonatomic,strong) NSString * strDealId;
@property NSInteger is_from;
@end
