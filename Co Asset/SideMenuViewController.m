//
//  SideMenuViewController.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuObject.h"
#import "SideMenuTableViewCell.h"
#import "Utility.h"
#import "ObjAuth.h"
#import "StringTable.h"
@interface SideMenuViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (nonatomic,retain) NSMutableArray * arrList;
@end

@implementation SideMenuViewController
- (NSMutableArray *)arrList{
    if (!_arrList) {
        _arrList = [[NSMutableArray alloc] init];
    }
    return _arrList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"3c3c3c"]];
    [self hardCodeMenuList];
    [self.tbl reloadData];
    [self.tbl setBackgroundColor:[UIColor clearColor]];
    //self.tbl.separatorColor = [UIColor lightGrayColor];
    
    UIView *v = [[UIView alloc] init];
    self.tbl.tableFooterView = v;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    self.tbl.separatorColor = [UIColor darkGrayColor];
    self.tbl.separatorInset = UIEdgeInsetsZero;
    
    
}

- (void)slidingViewControllerWillShowLeftView:(ADSlidingViewController *)slidingViewController{
    NSLog(@"side menu show!!!");
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    [self setSignInOut:obj.access_token];
}

- (void) setSignInOut:(NSString *)access_token{
    if (![Utility stringIsEmpty:access_token shouldCleanWhiteSpace:YES]) {
        if ([self.arrList count] == 4) {
            SideMenuObject * side = [self.arrList objectAtIndex:3];
            side.strName = @"Sign Out";
            [self.arrList replaceObjectAtIndex:3 withObject:side];
            [self.tbl  reloadData];
        }
    }
    else{
        if ([self.arrList count] == 4) {
            SideMenuObject * side = [self.arrList objectAtIndex:3];
            side.strName = @"Sign In";
            [self.arrList replaceObjectAtIndex:3 withObject:side];
            [self.tbl  reloadData];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)viewDidAppear:(BOOL)animated{
    ObjAuth * objAuth = [ObjAuth getCurrentAuth];
     if (![Utility stringIsEmpty:objAuth.access_token shouldCleanWhiteSpace:YES]) {
     [self setRouteToViewControllerId:PROFILE_VIEW_CONTROLLER];
     }
     else{
     [self setRouteToViewControllerId:SIGNIN_VIEW_CONTROLLER];
     }
     
     [self.view bringSubviewToFront:self.tbl];
}

- (void)hardCodeMenuList{
    SideMenuObject * sideObject = [[SideMenuObject alloc]init];
    sideObject.idx = 1;
    sideObject.strName = @"Profile";
    sideObject.strImageName = @"profile_icon";
    [self.arrList addObject:sideObject];
    
    sideObject = [SideMenuObject new];
    sideObject.idx = 2;
    sideObject.strName = @"Deals";
    sideObject.strImageName = @"deal_icon";
    [self.arrList addObject:sideObject];
    
    sideObject = [SideMenuObject new];
    sideObject.idx = 3;
    sideObject.strName = @"News";
    sideObject.strImageName = @"news_icon";
    [self.arrList addObject:sideObject];
    
    sideObject = [SideMenuObject new];
    sideObject.idx = 4;
    sideObject.strName = @"Sign In";
    sideObject.strImageName = @"sign_in_icon";
    [self.arrList addObject:sideObject];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SideMenuCell";
	SideMenuTableViewCell *cell = (SideMenuTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SideMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    SideMenuObject * obj = [self.arrList   objectAtIndex:indexPath.row];
    [cell viewLoadWithSideMenu:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 32;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"side menu selected!!");
    if (indexPath.row==0) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"NavProfile"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }
    else if (indexPath.row==1) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"NavDeal"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }
    else if(indexPath.row == 2){
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavNews"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }
    else if(indexPath.row == 3){
        ///ObjAuth * obj = [ObjAuth getCurrentAuth];
        ObjAuth * obj = [ObjAuth removeCurrentAuth];
        [self setSignInOut:obj.access_token];
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavLogin"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (void) setRouteToViewControllerId:(NSInteger )index{
    /*NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self tableView:nil didSelectRowAtIndexPath:indexPath];*/
    if (index==0) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"NavProfile"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
    }
    else if (index==1) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"NavDeal"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
    }
    else if(index== 2){
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavNews"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
    }
    else if(index == 3){
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavLogin"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

@end
