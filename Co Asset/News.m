//
//  News.m
//  Co Asset
//
//  Created by Zayar on 5/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "News.h"


@implementation News

@dynamic idx;
@dynamic title;
@dynamic author;
@dynamic date;
@dynamic section_name;
@dynamic detail_content;
@dynamic graphic_content;

@end
