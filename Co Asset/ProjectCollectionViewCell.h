//
//  NewsCollectionViewCell.h
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjProject.h"
@interface ProjectCollectionViewCell : UICollectionViewCell

- (void) setupCell;
- (void) loadCell;
- (void) loadCellWithObjProject:(ObjProject *)obj;
@end
