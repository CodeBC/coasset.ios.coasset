//
//  NewsCollectionViewCell.m
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProjectCollectionViewCell.h"
#import "UIImageView+WebCache.h"
@interface ProjectCollectionViewCell()
@property (nonatomic,strong) UILabel * lblTitle;
@property (nonatomic,strong) UIImageView * imgThumbView;
@end
@implementation ProjectCollectionViewCell

- (UIImageView *)imgThumbView{
    if (!_imgThumbView) {
        _imgThumbView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 58, 50)];
    }
    return _imgThumbView;
}

- (UILabel *) lblTitle{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20+50, 0, 200-50, 50)];
        _lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:11];
        _lblTitle.numberOfLines = 0;
    }
    return _lblTitle;
}

- (void) setupCell{
    [self addSubview:self.imgThumbView];
    [self addSubview:self.lblTitle];
}

- (void) loadCellWithObjProject:(ObjProject *)obj{
    self.lblTitle.text = obj.project.name;
    [self.imgThumbView setImageWithURL:[NSURL URLWithString:obj.project.photo] placeholderImage:nil];
}

@end
