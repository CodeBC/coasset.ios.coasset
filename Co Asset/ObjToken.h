//
//  ObjToken.h
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjToken : NSObject
@property (nonatomic,strong) NSString * strProductCode;
@property (nonatomic,strong) NSString * strName;
@property (nonatomic,strong) NSString * strCurrency;
@property CGFloat price;
+ (NSArray *)parseProductTokenWithArray:(NSArray *)arr;
@end
