//
//  ProfileProjectCateViewController.m
//  Co Asset
//
//  Created by Zayar on 5/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProfileProjectCateViewController.h"
#import "Utility.h"
#import "Profile.h"
#import "Account_Type.h"
#import "Tokens.h"
#import "ObjProjectCate.h"
#import "ProjectCateCell.h"
#import "StringTable.h"
#import "ParticipantProjectViewController.h"
#import "TokenViewController.h"
@interface ProfileProjectCateViewController ()
{

}
@property (strong, nonatomic) IBOutlet UICollectionView *projectCateCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountType;
@property (strong, nonatomic) IBOutlet UILabel *lblToken;
@property (strong, nonatomic) NSMutableArray * arrList;
@property (strong, nonatomic) ParticipantProjectViewController * participantProjectViewController;
@property (strong, nonatomic) TokenViewController * tokenViewController;
@end

@implementation ProfileProjectCateViewController
@synthesize objUser;

- (ParticipantProjectViewController *) participantProjectViewController{
    if (!_participantProjectViewController) {
        _participantProjectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ParticipantProject"];
    }
    return _participantProjectViewController;
}

- (TokenViewController *) tokenViewController{
    if (!_tokenViewController) {
        _tokenViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TokenViewController"];
    }
    return _tokenViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self hardCodeMenuList];
    [self.projectCateCollectionView reloadData];
    [self.projectCateCollectionView setBackgroundColor:[UIColor clearColor]];
    [self setNavBarTitle:@"PROJECTS"];
}

- (NSMutableArray *)arrList{
    if (!_arrList) {
        _arrList = [[NSMutableArray alloc] init];
    }
    return _arrList;
}

- (void)hardCodeMenuList{
    ObjProjectCate * obj = [[ObjProjectCate alloc]init];
    obj.idx = 1;
    obj.strName = @"Your Participated Projects";
    obj.strImageName = @"own_prj_bg";
    obj.strDescription = @"find a project";
    [self.arrList addObject:obj];
    
    obj = [ObjProjectCate new];
    obj.idx = 2;
    obj.strName = @"Your Owned Projects";
    obj.strImageName = @"participant_bg";
    obj.strDescription = @"find a project";
    [self.arrList addObject:obj];
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadTheViewsWith:objUser];
}

- (void) loadTheViewsWith:(ObjUser *)obj{
    [self setAccountTypeValue:obj.user.account_type.account_type];
    [self setTokenValue:[NSString stringWithFormat:@"%d",[obj.user.tokens.qty integerValue]]];
}

- (void)setAccountTypeValue:(NSString *)str{
    if ([Utility stringIsEmpty:str shouldCleanWhiteSpace:YES]) {
        str = @"-";
    }
    self.lblAccountType.text = [NSString stringWithFormat:@"Account Type: %@",str];
}

- (void)setTokenValue:(NSString *)str{
    if ([Utility stringIsEmpty:str shouldCleanWhiteSpace:YES]) {
        str = @"0";
    }
    self.lblToken.text = [NSString stringWithFormat:@"Token: %@",str];
}

- (IBAction)onToken:(UIButton *)sender {
    [self.navigationController pushViewController:self.tokenViewController animated:YES];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProjectCateCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ProjectCateCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 5;
    [cell setupCell];
    [cell loadCellWithObjCate:[self.arrList objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*NewsDetailViewController * NewsDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
    ObjNews * obj = [arrNews objectAtIndex:indexPath.row];
    NewsDetailVC.objNews = obj;
    [self.navigationController pushViewController:NewsDetailVC animated:YES];*/
    ObjProjectCate * obj = [self.arrList objectAtIndex:indexPath.row];
    self.participantProjectViewController.objUser = objUser;
    if (obj.idx == 1) {
        self.participantProjectViewController.is_from = 0;
        
        [self.navigationController pushViewController:self.participantProjectViewController animated:YES];
    }
    else if(obj.idx == 2){
        self.participantProjectViewController.is_from = 1;
        [self.navigationController pushViewController:self.participantProjectViewController animated:YES];
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(PROJECT_CATE_CELL_WIDTH, PROJECT_CATE_CELL_HEIGHT);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 70, 1, 70);
}

@end
