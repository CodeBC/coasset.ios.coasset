//
//  WebDescriptionView.h
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WebDescriptionViewDelegate;
@interface WebDescriptionView : UIView
@property (nonatomic, strong) id<WebDescriptionViewDelegate> owner;
- (void) loadWithWebString:(NSString *)strWeb;
@end
@protocol WebDescriptionViewDelegate
- (void) webDescriptionDidFinish:(WebDescriptionView *) webDescriptionView andWebHeight:(CGSize)webSize;
@end
