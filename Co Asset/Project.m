//
//  Project.m
//  Co Asset
//
//  Created by Zayar on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Project.h"


@implementation Project

@dynamic address_1;
@dynamic address_2;
@dynamic city;
@dynamic country;
@dynamic deal_id;
@dynamic developer;
@dynamic idx;
@dynamic name;
@dynamic photo;
@dynamic project_type;
@dynamic state_region;
@dynamic short_description;

@end
