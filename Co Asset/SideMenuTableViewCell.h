//
//  SideMenuTableViewCell.h
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuObject.h"
@interface SideMenuTableViewCell : UITableViewCell
- (void)setUpViews;
- (void)viewLoadWithSideMenu:(SideMenuObject *)obj;
@end
