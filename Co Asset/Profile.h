//
//  Profile.h
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profile : NSManagedObject

@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * country_prefix;
@property (nonatomic, retain) NSString * cell_phone;
@property (nonatomic, retain) NSString * address_1;
@property (nonatomic, retain) NSString * address_2;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * region_state;
@property (nonatomic, retain) NSString * country;

@end
