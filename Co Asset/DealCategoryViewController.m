//
//  DealCategoryViewController.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DealCategoryViewController.h"
#import "ObjDealCate.h"
#import "DealCateTableViewCell.h"
#import "StringTable.h"
#import "OnGoingDealsViewController.h"
#import "RecentlyClosedDealViewController.h"
#import "ProfileProjectCateViewController.h"
@interface DealCategoryViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (nonatomic,strong) NSMutableArray * arrList;
@end

@implementation DealCategoryViewController

- (NSMutableArray *)arrList{
    if (!_arrList) {
        _arrList = [[NSMutableArray alloc] init];
    }
    return _arrList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"DEALS"];
    [self setNavMenuButton];
    [self hardCodeMenuList];
    [self.tbl reloadData];
    [self.tbl setBackgroundColor:[UIColor clearColor]];
}

- (void) onMenu:(UIBarButtonItem *)sender{
    [[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)hardCodeMenuList{
    ObjDealCate * obj = [[ObjDealCate alloc]init];
    obj.idx = 1;
    obj.strName = @"Ongoing Deals";
    obj.strImageName = @"ongoing_deal_bg";
    [self.arrList addObject:obj];
    
    obj = [ObjDealCate new];
    obj.idx = 2;
    obj.strName = @"Recently Closed Deals";
    obj.strImageName = @"recent_closed_deal_bg";
    [self.arrList addObject:obj];
    
    obj = [ObjDealCate new];
    obj.idx = 2;
    obj.strName = @"My Deals";
    obj.strImageName = @"my_deal_bg";
    [self.arrList addObject:obj];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SideMenuCell";
	DealCateTableViewCell *cell = (DealCateTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[DealCateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    ObjDealCate * obj = [self.arrList   objectAtIndex:indexPath.row];
    [cell viewLoadWithDealCate:obj];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEAL_CATE_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*if (indexPath.row==1) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"NavDeal"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }
    else if(indexPath.row == 2){
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavNews"];
        ADSlidingViewController *slidingViewController = [self slidingViewController];
        [slidingViewController setMainViewController:nav];
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }*/
    if (indexPath.row == 0) {
        OnGoingDealsViewController * ongoingDealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OngoingDeal"];
        [self.navigationController pushViewController:ongoingDealViewController animated:YES];
    }
    else if (indexPath.row == 1) {
        RecentlyClosedDealViewController * detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RecentlyClosedDeal"];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else if (indexPath.row == 2) {
        ProfileProjectCateViewController * detailViewController = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"ProjectCate"];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}


@end
