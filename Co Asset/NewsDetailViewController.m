//
//  NewsDetailViewController.m
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
@interface NewsDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblNewsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsTime;
@property (weak, nonatomic) IBOutlet UIScrollView *custromSlider;
@property (strong, nonatomic) IBOutlet UIWebView *webNewsDetail;

@end

@implementation NewsDetailViewController
{
    UIPageControl * pageControl;
}
@synthesize objNews;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self populateImages];
    [self setNavBarTitle:@"NEWS DETAILS"];
}

- (void)viewWillAppear:(BOOL)animated{
    
    //[self reloadTheContentViewsWith:objNews];
    [self syncNewsDetailWithNews:objNews];
}

- (void)reloadTheContentViewsWith:(ObjNews *)obj{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",obj.news.idx];
    News * newsDetail = [News MR_findFirstWithPredicate:predicate inContext:localContext];
    objNews.news = newsDetail;
    self.lblNewsTitle.text = obj.news.title;
    self.lblNewsTime.text = [obj getHumanDate];
    [self.webNewsDetail loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"width: %fpx;\"><font face=\"AvenirNext-Regular\" size=\"2\" color='#000'> <br/> %@ </font></body></html>",self.webNewsDetail.frame.size.width - 10, obj.news.detail_content] baseURL:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) syncNewsDetailWithNews:(ObjNews *)obj{
    [SVProgressHUD show];
    NSString * strNewsDetailLink = NEWS_LINK;
    strNewsDetailLink = [strNewsDetailLink stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",obj.news.idx]];
    [[CoAssetAPIClient sharedClient] POST:[NSString stringWithFormat:@"%@",strNewsDetailLink] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"syncNewsDetail successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        [ObjNews setNewsDetailSaveAndUpdateFromDic:myDict];
        [self reloadTheContentViewsWith:obj];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
