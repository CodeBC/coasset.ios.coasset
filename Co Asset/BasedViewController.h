//
//  BasedViewController.h
//  Co Asset
//
//  Created by Zayar on 5/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasedViewController : UIViewController
- (void)setNavBarTitle:(NSString *)str;
- (void)setNavMenuButton;
@end
