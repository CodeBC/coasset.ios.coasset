//
//  ProfileViewController.m
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProfileViewController.h"
#import "Utility.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjUser.h"
#import "ObjAuth.h"
#import "Account_Type.h"
#import "ChangePasswordViewController.h"
#import "PersonalInfoViewController.h"
#import "ProfileProjectCateViewController.h"
#import "TokenViewController.h"

@interface ProfileViewController ()
{
    ObjUser * currentUser;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgProfileView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountType;
@property (strong, nonatomic) IBOutlet UILabel *lblToken;
@property (strong, nonatomic) IBOutlet UIButton *btnChgPass;
@property (strong, nonatomic) IBOutlet UIButton *btnPersonInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnProjects;
@property (strong, nonatomic) ChangePasswordViewController * changePasswordViewController;
@property (strong, nonatomic) PersonalInfoViewController * personalInfoViewController;
@property (strong, nonatomic) ProfileProjectCateViewController * profileProjectCateViewController;
@property (strong, nonatomic) TokenViewController * tokenViewController;
@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"PROFILE"];
    [self setNavMenuButton];
    
    [Utility makeCornerRadius:self.imgProfileView andRadius:3];
    [Utility makeCornerRadius:self.btnChgPass andRadius:5];
    [Utility makeCornerRadius:self.btnPersonInfo andRadius:5];
    [Utility makeCornerRadius:self.btnProjects andRadius:5];
    
    
}

- (ChangePasswordViewController *) changePasswordViewController{
    if (!_changePasswordViewController) {
        _changePasswordViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePass"];
    }
    return _changePasswordViewController;
}

- (PersonalInfoViewController *) personalInfoViewController{
    if (!_personalInfoViewController) {
        _personalInfoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfo"];
    }
    return _personalInfoViewController;
}

- (TokenViewController *) tokenViewController{
    if (!_tokenViewController) {
        _tokenViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TokenViewController"];
    }
    return _tokenViewController;
}

- (ProfileProjectCateViewController *) profileProjectCateViewController{
    if (!_profileProjectCateViewController) {
        _profileProjectCateViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectCate"];
    }
    return _profileProjectCateViewController;
}

- (IBAction)onToken:(UIButton *)sender {
    [self.navigationController pushViewController:self.tokenViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"ProfileViewController view will appear!!");
    [self syncProfile];
}

- (void) onMenu:(UIBarButtonItem *)sender{
    [[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void) reloadTheViewWithObjUser:(ObjUser *)obj{
    currentUser = obj;
    self.lblName.text = [ObjUser setUserFullName:obj];
    self.lblAccountType.attributedText = [ObjUser setAccountName:obj];
    self.lblToken.text = [ObjUser setToken:obj];
    
}

- (IBAction)onChgPasss:(UIButton *)sender {
    [self.navigationController pushViewController:self.changePasswordViewController animated:YES];
}

- (IBAction)onPersonInfo:(UIButton *)sender {
    self.personalInfoViewController.objUser = currentUser;
    [self.navigationController pushViewController:self.personalInfoViewController animated:YES];
}

- (IBAction)onProjects:(UIButton *)sender {
    self.profileProjectCateViewController.objUser = currentUser;
    [self.navigationController pushViewController:self.profileProjectCateViewController animated:YES];
}

- (void) syncProfile{
    [SVProgressHUD show];
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    NSLog(@"token %@, token type %@",obj.access_token,obj.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:obj andLink:PROFILE_LINK] GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncProfile successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        ObjUser * obj = [ObjUser setUserSaveAndUpdateFromDic:myDict];
        /*NSArray * arr = (NSArray *)myDict;
        NSLog(@"syncProfile count %d",[arr count]);
        for(NSInteger i=0;i<[arr count];i++){
            //[ObjDeal setDealSaveAndUpdateFromDic:[arr objectAtIndex:i]];
        }*/
        [self reloadTheViewWithObjUser:obj];
        [SVProgressHUD dismiss];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
