//
//  ObjAuth.h
//  Co Asset
//
//  Created by Zayar on 5/19/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+RMArchivable.h"
#import "RMMapper.h"
@interface ObjAuth : NSObject<RMMapping>
@property (nonatomic, retain) NSString * access_token;
@property (nonatomic, retain) NSString * refresh_token;
@property (nonatomic, retain) NSString * scope;
@property (nonatomic, retain) NSString * token_type;
@property (nonatomic, retain) NSString * password;
@property NSInteger expires_timetick;

+(ObjAuth *)getCurrentAuth;
+(void)setCurrentAuth:(ObjAuth *)obj;
+(ObjAuth *)removeCurrentAuth;
@end
