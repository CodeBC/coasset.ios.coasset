//
//  TokenViewCell.m
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "TokenViewCell.h"
@interface TokenViewCell()
@property (strong,nonatomic) UILabel * lblName;
@property (strong,nonatomic) UILabel * lblValue;
@end
@implementation TokenViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 120, 30)];
        _lblName.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    }
    return _lblName;
}

- (UILabel *)lblValue{
    if (!_lblValue) {
        _lblValue = [[UILabel alloc] initWithFrame:CGRectMake(self.lblName.frame.origin.x + self.lblName.frame.size.width+5, 5, 120, 30)];
        _lblValue.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        _lblValue.textAlignment = NSTextAlignmentRight;
    }
    return _lblValue;
}

- (void) setUpViews{
    [self addSubview:self.lblName];
    [self addSubview:self.lblValue];
}

- (void)viewLoadWith:(ObjToken *)objToken{
    self.lblName.text = objToken.strName;
    self.lblValue.text = [NSString stringWithFormat:@"%.2f %@",objToken.price,objToken.strCurrency];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
