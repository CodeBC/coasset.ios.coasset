//
//  User.m
//  Co Asset
//
//  Created by Zayar on 5/28/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "User.h"
#import "Account_Type.h"
#import "Profile.h"
#import "Tokens.h"


@implementation User

@dynamic email;
@dynamic first_name;
@dynamic idx;
@dynamic last_name;
@dynamic username;
@dynamic password;
@dynamic account_type;
@dynamic profile;
@dynamic tokens;

@end
