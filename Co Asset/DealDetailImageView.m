//
//  DealDetailImageView.m
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DealDetailImageView.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
@interface DealDetailImageView()
@property (nonatomic,strong) UIImageView * imgView;
@property (nonatomic,strong) UIView * lblBgView;
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblType;
@property (nonatomic,strong) UILabel * lblDate;
@end
@implementation DealDetailImageView

- (UIImageView *) imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
    }
    return _imgView;
}

- (UIView *) lblBgView{
    if (!_lblBgView) {
        _lblBgView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.size.height-DEAL_IMAGE_LABEL_HEIGHT, self.frame.size.width, DEAL_IMAGE_LABEL_HEIGHT)];
        [_lblBgView setBackgroundColor:[UIColor darkTextColor]];
        [_lblBgView setAlpha:0.4];
    }
    return _lblBgView;
}

- (UILabel *) lblName{
    if (!_lblName){
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.size.height-DEAL_IMAGE_LABEL_HEIGHT, self.frame.size.width-100, DEAL_IMAGE_LABEL_HEIGHT)];
        _lblName.textColor = [UIColor whiteColor];
        _lblName.backgroundColor = [UIColor clearColor];
        _lblName.font = [UIFont systemFontOfSize:12];
        _lblName.textAlignment = NSTextAlignmentLeft;
    }
    return _lblName;
}

- (UILabel *) lblType{
    if (!_lblType) {
        _lblType = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-DEAL_IMAGE_CATE_LABEL_WIDTH, 10, DEAL_IMAGE_CATE_LABEL_WIDTH, DEAL_IMAGE_LABEL_HEIGHT)];
        _lblType.textColor = [UIColor whiteColor];
        _lblType.backgroundColor = [UIColor colorWithHexString:@"d24a4a"];
        _lblType.font = [UIFont systemFontOfSize:12];
        _lblType.textAlignment = NSTextAlignmentCenter;
    }
    return _lblType;
}

- (UILabel *) lblDate{
    if (!_lblDate) {
        _lblDate = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-DEAL_IMAGE_CATE_LABEL_WIDTH, self.frame.size.height-DEAL_IMAGE_LABEL_HEIGHT, self.frame.size.width-200, DEAL_IMAGE_LABEL_HEIGHT)];
        _lblDate.textColor = [UIColor whiteColor];
        _lblDate.backgroundColor = [UIColor clearColor];
        _lblDate.font = [UIFont systemFontOfSize:12];
        _lblDate.textAlignment = NSTextAlignmentRight;
    }
    return _lblDate;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setUpViews{
    [self addSubview:self.imgView];
    [self addSubview:self.lblBgView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblDate];
    [self addSubview:self.lblType];
}

- (void)loadTheViewWithDeal:(ObjDeal *)obj{
    NSLog(@"project photo %@",obj.project.photo);
    [self.imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",obj.project.photo]] placeholderImage:[UIImage imageNamed:@"ongoing_deal_bg"]];
    [self.lblName setText:[self setInvestorCount:[obj.deal.interested integerValue]]];
    [self.lblType setText:[self setInvestorCount:[obj.deal.time_horizon integerValue]]];
    [self.lblType setText:obj.deal.offer_type];
}

- (NSString *)setInvestorCount:(NSInteger) count{
    return [NSString stringWithFormat:@"Interested Investor: %d",count];
}

- (NSString *)setDayCount:(NSInteger) count{
    return [NSString stringWithFormat:@"%d days left",count];
}

@end
