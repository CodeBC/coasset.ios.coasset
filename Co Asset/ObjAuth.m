//
//  ObjAuth.m
//  Co Asset
//
//  Created by Zayar on 5/19/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ObjAuth.h"
#import "StringTable.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
@implementation ObjAuth
@synthesize access_token,refresh_token,scope,token_type,expires_timetick,password;
+(ObjAuth *)getCurrentAuth{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    ObjAuth * obj = [prefs rm_customObjectForKey:SIGN_IN_LINK];
    return obj;
}

+(void)setCurrentAuth:(ObjAuth *)obj{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    [prefs rm_setCustomObject:obj forKey:SIGN_IN_LINK];
    [prefs synchronize];
}

+(ObjAuth *)removeCurrentAuth{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    ObjAuth * obj = [prefs rm_customObjectForKey:SIGN_IN_LINK];
    obj.access_token = @"";
    [prefs rm_setCustomObject:obj forKey:SIGN_IN_LINK];
    [prefs synchronize];
    return obj;
}

- (NSDictionary *)rm_dataKeysForClassProperties
{
    return @{
             @"access_token" : @"access_token",
             @"refresh_token" : @"refresh_token",
             @"scope" : @"scope",
              @"token_type" : @"token_type"
             };
}

- (NSArray *)rm_excludedProperties
{
    return @[@"display"];
}

@end
