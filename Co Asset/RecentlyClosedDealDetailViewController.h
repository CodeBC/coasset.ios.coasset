//
//  RecentlyClosedDealDetailViewController.h
//  Co Asset
//
//  Created by Zayar on 5/16/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDeal.h"
@interface RecentlyClosedDealDetailViewController : BasedViewController
@property (nonatomic,strong) ObjDeal * objDeal;
@end
