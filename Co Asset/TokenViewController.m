//
//  TokenViewController.m
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "TokenViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjToken.h"
#import "TokenViewCell.h"
#import "Utility.h"
#import "TokenWebViewController.h"
@interface TokenViewController ()
{
    NSArray * arrTokens;
}
@property (strong, nonatomic) IBOutlet UICollectionView *tokenCollectionView;
@property (nonatomic, strong) TokenWebViewController * tokenWebViewController;

@end

@implementation TokenViewController

- (TokenWebViewController *) tokenWebViewController{
    if (!_tokenWebViewController) {
        _tokenWebViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TokenWebView"];
    }
    return _tokenWebViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"TOKENS"];
    [self.tokenCollectionView setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncProductToken];
}

- (void)syncProductToken{
    [SVProgressHUD show];
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    NSLog(@"token %@, token type %@",obj.access_token,obj.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:obj andLink:@""] GET:[NSString stringWithFormat:@"%@",TOKENS_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncProfile successfully return!!! %@",json);
        
        NSArray * arrTemp = (NSArray *)json;
        arrTokens = [ObjToken parseProductTokenWithArray:arrTemp];
        [self.tokenCollectionView reloadData];
        
        [SVProgressHUD dismiss];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrTokens count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TokenViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"TokenViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 5;
    [cell setUpViews];
    [cell viewLoadWith:[arrTokens objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ObjToken * obj = [arrTokens objectAtIndex:indexPath.row];
    [self syncTokenId:obj.strProductCode];
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(PROJECT_LIST_CELL_WIDTH, 40);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 70, 1, 70);
}

- (void)syncTokenId:(NSString *)strTokenId{
    [SVProgressHUD show];
    ObjAuth * objA = [ObjAuth getCurrentAuth];
    
    NSDictionary * param =  @{@"product_code":strTokenId};
    
    [[CoAssetAPIClient sharedClientWithHeader:objA andLink:@""] POST:[NSString stringWithFormat:@"%@",TOKENS_PAYPAL_LINK] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"sync pass successfully return!!! %@",json);
        NSDictionary * dic = (NSDictionary *) json;
        NSString * strHtml = [dic objectForKey:@"html"];
        self.tokenWebViewController.strHtml = strHtml;
        [self.navigationController pushViewController:self.tokenWebViewController animated:YES];
        
        [SVProgressHUD dismiss];
        //[self.navigationController popViewControllerAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
    /*NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PERSONAL_INFO_LINK]
     cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
     [request setHTTPMethod:@"POST"];
     [request setValue:@"Basic: someValue" forHTTPHeaderField:@"Authorization"];
     [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
     [request setHTTPBody: param];
     
     AFHTTPSessionManager * requestManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:PERSONAL_INFO_LINK]];*/
    NSLog(@"param dic: %@",param);
}


@end
