//
//  OnGoingDealsViewController.m
//  Co Asset
//
//  Created by ark on 25/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "OnGoingDealsViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjDeal.h"
#import "DealOngoingTableViewCell.h"
#import "ObjDeal.h"
#import "OngoingDealDetailViewController.h"
#import "Deal.h"
@interface OnGoingDealsViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (nonatomic,strong) NSMutableArray * arrList;
@property (nonatomic,strong) OngoingDealDetailViewController * dealDetailViewController;
@end

@implementation OnGoingDealsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (OngoingDealDetailViewController *)dealDetailViewController{
    if (!_dealDetailViewController) {
        _dealDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OngoingDealDetail"];
    }
    return _dealDetailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavBarTitle:@"ONGOING DEALS"];
    [self.tbl reloadData];
    [self.tbl setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncOngoingDeal];
    //[self reloadTheViews];
}

- (void)reloadTheViews{
    self.arrList = [ObjDeal getDealArray];
    [self.tbl reloadData];
}

- (void) syncOngoingDeal{
    [SVProgressHUD show];
    [[CoAssetAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",ONGOING_DEAL_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncNews successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arr = (NSArray *)myDict;
        self.arrList = (NSMutableArray *)[ObjDeal parseDealsWithArray:arr];
        [self.tbl reloadData];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SideMenuCell";
	DealOngoingTableViewCell *cell = (DealOngoingTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[DealOngoingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    ObjDeal * obj = [self.arrList  objectAtIndex:indexPath.row];
    [cell viewLoadWithDealOngoing:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEAL_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjDeal * obj = [self.arrList  objectAtIndex:indexPath.row];
    //NSLog(@"obj unid %@",obj.deal.idx);
    self.dealDetailViewController.is_from = PROJECT_ALIVE;
    self.dealDetailViewController.objDeal = obj;
    self.dealDetailViewController.strDealId = obj.deal.idx;
    
    [self.navigationController pushViewController:self.dealDetailViewController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [ObjDeal deleteDeals];
}


@end
