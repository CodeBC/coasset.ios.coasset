//
//  LabelAndIconView.m
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LabelAndIconView.h"
@interface LabelAndIconView()
@property (nonatomic, strong) UILabel * lblCap;
@property (nonatomic, strong) UIImageView * imgIconView;
@end
@implementation LabelAndIconView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (UILabel *) lblCap{
    if (!_lblCap) {
        _lblCap = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 20 - 30, self.frame.size.height - 10)];
        _lblCap.backgroundColor = [UIColor clearColor];
        _lblCap.textColor = [UIColor blackColor];
        _lblCap.font = [UIFont systemFontOfSize:14];
        _lblCap.numberOfLines = 0;
    }
    return _lblCap;
}

- (UIImageView *) imgIconView{
    if (!_imgIconView) {
        _imgIconView = [[UIImageView alloc] initWithFrame:CGRectMake((self.lblCap.frame.origin.x + self.lblCap.frame.size.width) + 5, 15, 30, 40)];
        _imgIconView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgIconView;
}
//
- (void)setUpViews{
    [self addSubview:self.lblCap];
    //[self addSubview:self.imgIconView];
    //[self addSubview:self.lblName];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) loadWithCapName:(NSString *)strCapName andName:(NSString *)strName andIconName:(NSString *)strIcon{
    self.lblCap.text = [NSString stringWithFormat:@"%@ : %@",strCapName,strName];
    [self.imgIconView setImage:[UIImage imageNamed:strIcon]];
}

@end
