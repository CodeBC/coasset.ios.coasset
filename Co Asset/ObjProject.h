//
//  ObjProject.h
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"
@interface ObjProject : NSObject
@property (nonatomic,strong) Project * project;
+ (ObjProject *) setProjectSaveAndUpdateFromDic:(NSDictionary *)dic andDealId:(NSString *)dealId;
+ (NSArray *) setProjectForListSaveAndUpdateFromDic:(NSArray *)arr;
+ (Project *) parseProject:(NSDictionary *)dic andDealId:(NSString *)dealId;
@end
