//
//  NewsViewController.h
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : BasedViewController <UICollectionViewDataSource, UICollectionViewDelegate>
- (void) onMenu:(UIBarButtonItem *)sender;
@end
