//
//  WebDescriptionView.m
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "WebDescriptionView.h"
@interface WebDescriptionView()
@property (nonatomic,strong) UIWebView * webView;
@end
@implementation WebDescriptionView
@synthesize owner;
- (UIWebView *) webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width, self.frame.size.height - 10)];
        _webView.delegate = self;
    }
    return _webView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (void)setUpViews{
    [self addSubview:self.webView];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) loadWithWebString:(NSString *)strWeb{
    //NSLog(@"str web %@",strWeb);
    //NSString * strStyledWeb = [NSString stringWithFormat:strWeb,self.webView.frame.size.width];
    [self.webView loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"width: %fpx;\"><font face=\"Helvetica Neue\" size=\"2\" color='#000'> <br/> %@ </font></body></html>",self.webView.frame.size.width-10,strWeb] baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    NSLog(@"size: %f, %f", fittingSize.width, fittingSize.height);
    [owner webDescriptionDidFinish:self andWebHeight:fittingSize];
}

@end
