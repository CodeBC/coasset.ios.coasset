//
//  ObjUser.h
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface ObjUser : NSObject
@property (nonatomic, strong) User * user;
+ (ObjUser *) setUserSaveAndUpdateFromDic:(NSDictionary *)dic;
+ (ObjUser*) sharedClient;
+ (NSString *) setUserFullName:(ObjUser *)obj;
+ (NSAttributedString *) setAccountName:(ObjUser *)obj;
+ (NSString *) setToken:(ObjUser *)obj;
@end
