//
//  PersonalInfoViewController.m
//  Co Asset
//
//  Created by Zayar on 5/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "Utility.h"
#import "Profile.h"
#import "ObjAuth.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "Utility.h"
#import "AFHTTPSessionManager.h"
@interface PersonalInfoViewController ()
{
    
    IBOutlet UIScrollView *scrollView;
    ObjUser * objUser;
}
@property (strong, nonatomic) IBOutlet UITextField *txtFName;
@property (strong, nonatomic) IBOutlet UITextField *txtLName;
@property (strong, nonatomic) IBOutlet UIView *viewTextBg;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;

@end

@implementation PersonalInfoViewController
@synthesize objUser;
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"PERSONAL INFO"];
    [Utility makeCornerRadius:self.viewTextBg andRadius:5];
    [Utility makeCornerRadius:self.btnUpdate andRadius:5];
    [self textFieldSetup];
    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
}

- (void) textFieldSetup{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (IBAction)onUpdate:(UIButton *)sender {
    objUser.user.first_name = self.txtFName.text;
    objUser.user.last_name= self.txtLName.text;
    objUser.user.email = self.txtEmail.text;
    objUser.user.profile.cell_phone = self.txtPhone.text;
    NSLog(@"user country_prefix %@",objUser.user.profile.country_prefix);
    [self syncPersonInfo:objUser];
}

- (void)viewWillAppear:(BOOL)animated{
    [self registerForKeyboardNotifications];
    [self setPersonInfo:objUser];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self deregisterFromKeyboardNotifications];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void) scrollToOriginal{
    NSLog(@"scrollView.contentSize height %f",scrollView.contentSize.height);
    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFName) {
        [self.txtFName resignFirstResponder];
        [self.txtLName becomeFirstResponder];
    }else if (textField == self.txtLName){
        [self.txtLName resignFirstResponder];
        [self.txtPhone becomeFirstResponder];
    }
    else if(textField == self.txtPhone){
        [self.txtPhone resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    }
    else if(textField == self.txtEmail){
        [self.txtEmail resignFirstResponder];
    }
    return YES;
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
}

- (void)setPersonInfo:(ObjUser *)obj{
    objUser = obj;
    self.txtFName.text = obj.user.first_name;
    self.txtLName.text = obj.user.last_name;
    self.txtEmail.text = obj.user.email;
    self.txtPhone.text = obj.user.profile.cell_phone;
}

-(void) keyboardWillHide:(NSNotification *) notification {
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:self.viewTextBg];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    [self scrollToOriginal];
}

- (void)registerForKeyboardNotifications
{
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)syncPersonInfo:(ObjUser *)obj{
    [SVProgressHUD show];
    ObjAuth * objA = [ObjAuth getCurrentAuth];
    
    NSDictionary * dicProfile = @{@"country_prefix":obj.user.profile.country_prefix,@"cell_phone":obj.user.profile.cell_phone};
    NSDictionary * param =  @{@"username":obj.user.username,@"email":obj.user.email, @"password":@"12345",@"last_name":obj.user.last_name,@"first_name":obj.user.first_name,@"profile":dicProfile};
    NSLog(@"token %@, token type %@",objA.access_token,objA.token_type);

    [[CoAssetAPIClient sharedClientWithHeader:objA andLink:@""] PUT:[NSString stringWithFormat:@"%@",PERSONAL_INFO_LINK] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"sync pass successfully return!!! %@",json);
        [Utility showAlert:APP_TITLE message:@"Personal Info successfully changed."];
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
    /*NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PERSONAL_INFO_LINK]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"Basic: someValue" forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: param];
    
    AFHTTPSessionManager * requestManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:PERSONAL_INFO_LINK]];*/
    NSLog(@"param dic: %@",param);
}

- (void)viewDidDisappear:(BOOL)animated{
    self.txtFName.text = @"";
    self.txtLName.text = @"";
    self.txtEmail.text = @"";
    self.txtPhone.text = @"";
}

@end
