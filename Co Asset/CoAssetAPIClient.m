//
//  ChurchAPIClient.m
//  Church
//
//  Created by Zayar on 5/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "CoAssetAPIClient.h"
#import "StringTable.h"
@implementation CoAssetAPIClient

+ (instancetype)sharedClient {
    static CoAssetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:APP_BASED_LINK]];
        //[_sharedClient setSecurityPolicy:nil];
    });
    
    return _sharedClient;
}

+ (instancetype)sharedClientWithURL:(NSString *)strLink {
    static CoAssetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:strLink]];
        //[_sharedClient setSecurityPolicy:nil];
    });
    
    return _sharedClient;
}

+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andLink:(NSString *)strLink{
    static CoAssetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    NSLog(@"str link %@",strLink);
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:strLink]];
        _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
        [_sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",obj.token_type,obj.access_token] forHTTPHeaderField:@"Authorization"];
        [_sharedClient.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    });
    return _sharedClient;
}

+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andChPassLink:(NSString *)strLink{
    static CoAssetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    //NSLog(@"str link %@",strLink);
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:strLink]];
        [_sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",obj.token_type,obj.access_token] forHTTPHeaderField:@"Authorization"];
        //[_sharedClient setSecurityPolicy:nil];
        _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    return _sharedClient;
}

/*+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andLink:(NSString *)strLink{
    CoAssetAPIClient *_sharedClient = nil;
    _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:strLink]];
    [_sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",obj.token_type,obj.access_token] forHTTPHeaderField:@"Authorization"];
    //[_sharedClient setSecurityPolicy:nil];
    _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    return _sharedClient;
}

+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andChPassLink:(NSString *)strLink{
    CoAssetAPIClient *_sharedClient = nil;
    _sharedClient = [[CoAssetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:strLink]];
    [_sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",obj.token_type,obj.access_token] forHTTPHeaderField:@"Authorization"];
    //[_sharedClient setSecurityPolicy:nil];
    _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    return _sharedClient;
}*/

@end
