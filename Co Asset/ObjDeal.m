//
//  ObjDeal.m
//  Co Asset
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ObjDeal.h"
#import "Deal.h"
#import "Utility.h"
#import "ObjProject.h"
@implementation ObjDeal
@synthesize deal,project;
+ (ObjDeal*)sharedClient{
    static ObjDeal *_sharedClient = nil;
    
    _sharedClient = [[ObjDeal alloc] init];
    
    if(_sharedClient) {
        //_sharedClient = [[DBManager sharedClient] getConfig];
    }
    return _sharedClient;
}

- (id) init{
	if( self = [super init] ){
		project = [Project MR_createEntity];
        deal = [Deal MR_createEntity];
	}
	
	return self;
}


+ (ObjDeal *) setDealSaveAndUpdateFromDic:(NSDictionary *)dic{
    ObjDeal * objDeal = nil;
    if ([dic count]) {
        objDeal = [[ObjDeal alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * owner_type = [dic objectForKey:@"owner_type"];
        NSString * offer_type = [dic objectForKey:@"offer_type"];
        NSString * offer_title = [dic objectForKey:@"offer_title"];
        CGFloat min_annual_return=0.0;
        CGFloat max_annual_return =0.0;
        NSInteger time_horizon = 0;
        NSInteger interested = 0;
        NSInteger min_investment=0;
        NSString * currency = [dic objectForKey:@"currency"];
        NSString * short_description = [dic objectForKey:@"short_description"];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:owner_type shouldCleanWhiteSpace:YES]) owner_type = @"";
        if ([Utility stringIsEmpty:offer_type shouldCleanWhiteSpace:YES]) offer_type = @"";
        if ([Utility stringIsEmpty:offer_title shouldCleanWhiteSpace:YES]) offer_title = @"";
        if ([Utility stringIsEmpty:currency shouldCleanWhiteSpace:YES]) currency = @"";
        if ([Utility stringIsEmpty:short_description shouldCleanWhiteSpace:YES]) short_description = @"";
       
        
        if ([dic objectForKey:@"min_annual_return"] != [NSNull null]) {
            min_annual_return=[[dic objectForKey:@"min_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"max_annual_return"] != [NSNull null]) {
            max_annual_return=[[dic objectForKey:@"max_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"time_horizon"] != [NSNull null]) {
            time_horizon=[[dic objectForKey:@"time_horizon"] floatValue];
        }
        
        if ([dic objectForKey:@"interested"] != [NSNull null]) {
            interested=[[dic objectForKey:@"interested"] integerValue];
        }
        
        if ([dic objectForKey:@"min_investment"] != [NSNull null]) {
            min_investment=[[dic objectForKey:@"min_investment"] integerValue];
        }
        
    
        Deal * obj = [Deal MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.owner_type = owner_type;
            obj.offer_type = offer_type;
            obj.offer_title = offer_title;
            obj.currency = currency;
            obj.short_description = short_description;
            obj.min_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",min_annual_return]];
            obj.max_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",max_annual_return]];
            obj.time_horizon = [NSNumber numberWithInteger:time_horizon];
            obj.interested = [NSNumber numberWithInteger:interested];
            obj.min_investment = [NSNumber numberWithInteger:min_investment];
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
            objDeal.deal = obj;
        }
        else{
            Deal * obj2 = [Deal MR_createEntity];
            obj2.idx = strUniId;
            obj2.owner_type = owner_type;
            obj2.offer_type = offer_type;
            obj2.offer_title = offer_title;
            obj2.currency = currency;
            obj2.short_description = short_description;
            obj2.min_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",min_annual_return]];
            obj2.max_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",max_annual_return]];
            obj2.time_horizon = [NSNumber numberWithInteger:time_horizon];
            obj2.interested = [NSNumber numberWithInteger:interested];
            obj2.min_investment = [NSNumber numberWithInteger:min_investment];
            
            [obj2 MR_inContext:localContext];
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"Ongoing Deal saved");
            }];
            objDeal.deal = obj2;
        }
        
        NSDictionary * dicProject = [dic objectForKey:@"project"];
        [ObjProject setProjectSaveAndUpdateFromDic:dicProject andDealId:strUniId];
    }
    return objDeal;
}

+ (NSMutableArray *) getDealArray{
    NSMutableArray * arr=nil;
    arr = (NSMutableArray *)[Deal MR_findAll];
    NSMutableArray * arrNews = nil;
    if ([arr count]) {
        arrNews = [[NSMutableArray alloc] init];
    }
    
    for (Deal * obj in arr) {
        ObjDeal * objDeal = [[ObjDeal alloc] init];
        objDeal.deal = obj;
        NSLog(@"deal id %@",objDeal.deal.idx);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deal_id == %@",obj.idx];
        Project * project = [Project MR_findFirstWithPredicate:predicate];
        objDeal.project = project;
        [arrNews addObject:objDeal];
    }
    return arrNews;
}

+ (ObjDeal *) setDealDetailSaveAndUpdateFromDic:(NSDictionary *)dic{
    ObjDeal * objDeal = nil;
    if ([dic count]) {
        objDeal = [[ObjDeal alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * owner_type = [dic objectForKey:@"owner_type"];
        NSString * offer_type = [dic objectForKey:@"offer_type"];
        NSString * offer_title = [dic objectForKey:@"offer_title"];
        NSString * highlight = [dic objectForKey:@"highlight"];
        NSString * description = [dic objectForKey:@"description"];
        CGFloat min_annual_return=0.0;
        CGFloat max_annual_return =0.0;
        NSInteger time_horizon = 0;
        NSInteger interested = 0;
        NSInteger tokens_per_fancy = 0;
        NSInteger min_investment = 0;
        NSString * currency = [dic objectForKey:@"currency"];
        NSString * short_description = [dic objectForKey:@"short_description"];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:owner_type shouldCleanWhiteSpace:YES]) owner_type = @"";
        if ([Utility stringIsEmpty:offer_type shouldCleanWhiteSpace:YES]) offer_type = @"";
        if ([Utility stringIsEmpty:offer_title shouldCleanWhiteSpace:YES]) offer_title = @"";
        if ([Utility stringIsEmpty:currency shouldCleanWhiteSpace:YES]) currency = @"";
        if ([Utility stringIsEmpty:short_description shouldCleanWhiteSpace:YES]) short_description = @"";
        if ([Utility stringIsEmpty:highlight shouldCleanWhiteSpace:YES]) highlight = @"";
        if ([Utility stringIsEmpty:description shouldCleanWhiteSpace:YES]) description = @"";
        
        if ([dic objectForKey:@"min_annual_return"] != [NSNull null]) {
            min_annual_return=[[dic objectForKey:@"min_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"max_annual_return"] != [NSNull null]) {
            max_annual_return=[[dic objectForKey:@"max_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"time_horizon"] != [NSNull null]) {
            time_horizon=[[dic objectForKey:@"time_horizon"] floatValue];
        }
        
        if ([dic objectForKey:@"interested"] != [NSNull null]) {
            interested=[[dic objectForKey:@"interested"] integerValue];
        }
        
        if ([dic objectForKey:@"tokens_per_fancy"] != [NSNull null]) {
            tokens_per_fancy=[[dic objectForKey:@"tokens_per_fancy"] integerValue];
        }
        
        if ([dic objectForKey:@"min_investment"] != [NSNull null]) {
            min_investment=[[dic objectForKey:@"min_investment"] integerValue];
        }
        
        /*if (![Utility stringIsEmpty:[dic objectForKey:@"min_annual_return"] shouldCleanWhiteSpace:YES]) min_annual_return=[[dic objectForKey:@"min_annual_return"] floatValue];
         if (![Utility stringIsEmpty:[dic objectForKey:@"max_annual_return"] shouldCleanWhiteSpace:YES]) max_annual_return=[[dic objectForKey:@"max_annual_return"] floatValue];
         if (![Utility stringIsEmpty:[dic objectForKey:@"time_horizon"]  shouldCleanWhiteSpace:YES]) time_horizon=[[dic objectForKey:@"time_horizon"] integerValue];
         if (![Utility stringIsEmpty:[dic objectForKey:@"short_description"]  shouldCleanWhiteSpace:YES]) interested=[[dic objectForKey:@"short_description"] integerValue];*/
        
        Deal * obj = [Deal MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.owner_type = owner_type;
            obj.offer_type = offer_type;
            obj.offer_title = offer_title;
            obj.currency = currency;
            obj.short_description = short_description;
            obj.min_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",min_annual_return]];
            obj.max_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",max_annual_return]];
            obj.time_horizon = [NSNumber numberWithInteger:time_horizon];
            obj.interested = [NSNumber numberWithInteger:interested];
            obj.highlight = highlight;
            obj.long_description = description;
            obj.min_investment = [NSNumber numberWithInteger:min_investment];
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
            objDeal.deal = obj;
        }
        else{
            Deal * obj2 = [Deal MR_createEntity];
            obj2.idx = strUniId;
            obj2.owner_type = owner_type;
            obj2.offer_type = offer_type;
            obj2.offer_title = offer_title;
            obj2.currency = currency;
            obj2.short_description = short_description;
            obj2.min_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",min_annual_return]];
            obj2.max_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",max_annual_return]];
            obj2.time_horizon = [NSNumber numberWithInteger:time_horizon];
            obj2.interested = [NSNumber numberWithInteger:interested];
            obj2.highlight = highlight;
            obj2.long_description = description;
            obj2.min_investment = [NSNumber numberWithInteger:min_investment];
            
            [obj2 MR_inContext:localContext];
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"Ongoing Deal saved");
            }];
            objDeal.deal = obj2;
        }
        
        NSDictionary * dicProject = [dic objectForKey:@"project"];
       ObjProject * objPro = [ObjProject setProjectSaveAndUpdateFromDic:dicProject andDealId:strUniId];
        objDeal.project = objPro.project;
    }
    return objDeal;
}

+ (NSArray *) parseDealsWithArray:(NSArray *)arr{
    NSMutableArray * arrTemp = nil;
    if ([arr count]) {
        arrTemp = [[NSMutableArray alloc] init];
    }
    for (NSDictionary * dic in arr) {
        ObjDeal * objDeal = nil;
        if ([dic count]) {
            objDeal = [[ObjDeal alloc] init];
        }
        
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * owner_type = [dic objectForKey:@"owner_type"];
        NSString * offer_type = [dic objectForKey:@"offer_type"];
        NSString * offer_title = [dic objectForKey:@"offer_title"];
        CGFloat min_annual_return=0.0;
        CGFloat max_annual_return =0.0;
        NSInteger time_horizon = 0;
        NSInteger interested = 0;
        NSInteger min_investment=0;
        NSString * currency = [dic objectForKey:@"currency"];
        NSString * short_description = [dic objectForKey:@"short_description"];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:owner_type shouldCleanWhiteSpace:YES]) owner_type = @"";
        if ([Utility stringIsEmpty:offer_type shouldCleanWhiteSpace:YES]) offer_type = @"";
        if ([Utility stringIsEmpty:offer_title shouldCleanWhiteSpace:YES]) offer_title = @"";
        if ([Utility stringIsEmpty:currency shouldCleanWhiteSpace:YES]) currency = @"";
        if ([Utility stringIsEmpty:short_description shouldCleanWhiteSpace:YES]) short_description = @"";
        
        
        if ([dic objectForKey:@"min_annual_return"] != [NSNull null]) {
            min_annual_return=[[dic objectForKey:@"min_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"max_annual_return"] != [NSNull null]) {
            max_annual_return=[[dic objectForKey:@"max_annual_return"] floatValue];
        }
        
        if ([dic objectForKey:@"time_horizon"] != [NSNull null]) {
            time_horizon=[[dic objectForKey:@"time_horizon"] floatValue];
        }
        
        if ([dic objectForKey:@"interested"] != [NSNull null]) {
            interested=[[dic objectForKey:@"interested"] integerValue];
        }
        
        if ([dic objectForKey:@"min_investment"] != [NSNull null]) {
            min_investment=[[dic objectForKey:@"min_investment"] integerValue];
        }
        NSLog(@"min_investment value %d",min_investment);
        
        objDeal.deal.idx = strUniId;
        objDeal.deal.owner_type = owner_type;
        objDeal.deal.offer_type = offer_type;
        objDeal.deal.offer_title = offer_title;
        objDeal.deal.currency = currency;
        objDeal.deal.short_description = short_description;
        objDeal.deal.min_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",min_annual_return]];
        objDeal.deal.max_annual_return = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",max_annual_return]];
        objDeal.deal.time_horizon = [NSNumber numberWithInteger:time_horizon];
        objDeal.deal.interested = [NSNumber numberWithInteger:interested];
        objDeal.deal.min_investment = [NSNumber numberWithInteger:min_investment];
        objDeal.project = [ObjProject parseProject:[dic objectForKey:@"project"] andDealId:strUniId];
        [arrTemp addObject:objDeal];
    }
    return arrTemp;
}

+ (void) deleteDeals{
    [Deal MR_truncateAll];
    [Project MR_truncateAll];
}
@end
