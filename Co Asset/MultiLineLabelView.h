//
//  MultiLineLabelView.h
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiLineLabelView : UIView
- (void) setLine1Name:(NSString *)strCapName andName:(NSString *)strName;
- (void) setLine2Name:(NSString *)strCapName andName:(NSString *)strName;
- (void) setLine3Name:(NSString *)strCapName andName:(NSString *)strName;
- (void) setLine4Name:(NSString *)strCapName andName:(NSString *)strName;
- (void) setLine2Name:(NSString *)strCapName andName:(NSString *)strName andCurrencyCode:(NSString *)strCurrencyCode;
@end
