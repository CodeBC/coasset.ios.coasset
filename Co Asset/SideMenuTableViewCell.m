//
//  SideMenuTableViewCell.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SideMenuTableViewCell.h"
#import "SideMenuObject.h"
@interface SideMenuTableViewCell()

@property (nonatomic, strong) UIImageView * iconImgView;
@property (nonatomic, strong) UILabel * lblName;
@end
@implementation SideMenuTableViewCell

- (UIImageView *) iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 20, 20)];
        _iconImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _iconImgView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(20 + 10, 5, self.frame.size.width-100, 25)];
        _lblName.textColor = [UIColor whiteColor];
        _lblName.backgroundColor = [UIColor clearColor];
        _lblName.textAlignment = NSTextAlignmentLeft;
        _lblName.font = [UIFont systemFontOfSize:14];
    }
    return _lblName;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setUpViews{
    [self setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.iconImgView];
    [self addSubview:self.lblName];
}

- (void)viewLoadWithSideMenu:(SideMenuObject *)obj{
    self.iconImgView.image = [UIImage imageNamed:obj.strImageName];
    self.lblName.text = obj.strName;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
