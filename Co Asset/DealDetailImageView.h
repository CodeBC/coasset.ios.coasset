//
//  DealDetailImageView.h
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDeal.h"
@interface DealDetailImageView : UIView
- (void)setUpViews;
- (void)loadTheViewWithDeal:(ObjDeal *)obj;
@end
