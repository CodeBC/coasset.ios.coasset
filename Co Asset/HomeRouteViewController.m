//
//  HomeRouteViewController.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "HomeRouteViewController.h"
#import "SideMenuViewController.h"
#import "Utility.h"
@interface HomeRouteViewController ()

@property (nonatomic,strong) SideMenuViewController * sideMenuViewController;
@end

@implementation HomeRouteViewController
- (SideMenuViewController *)sideMenuViewController{
    if (!_sideMenuViewController) {
        _sideMenuViewController =[[UIStoryboard storyboardWithName:@"SideMenu" bundle:nil] instantiateViewControllerWithIdentifier:@"SideMenu"];
    }
    return _sideMenuViewController;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIViewController *mainvc;
    mainvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NavNews"];
	[[mainvc view] addGestureRecognizer:[self panGesture]];
    
	[self setMainViewController:mainvc];
    [self setLeftViewController:self.sideMenuViewController];
    [self setShowTopViewShadow:YES];
	[self setRightMainAnchorType:ADMainAnchorTypeResize];
    self.delegate = self;
}

- (void)slidingViewControllerWillShowLeftView:(ADSlidingViewController *)slidingViewController{
    //NSLog(@"side menu show!!!");
    [self.sideMenuViewController slidingViewControllerWillShowLeftView:slidingViewController];
}


@end
