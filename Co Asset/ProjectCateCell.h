//
//  ProjectCateCell.h
//  Co Asset
//
//  Created by Zayar on 6/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjProjectCate.h"
@interface ProjectCateCell : UICollectionViewCell
- (void) setupCell;
- (void) loadCellWithObjCate:(ObjProjectCate *)objCate;
@end
