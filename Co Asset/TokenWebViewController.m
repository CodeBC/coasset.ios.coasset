//
//  TokenWebViewController.m
//  Co Asset
//
//  Created by Zayar on 6/20/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "TokenWebViewController.h"

@interface TokenWebViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation TokenWebViewController
@synthesize strHtml;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.delegate = self;
    self.navigationItem.title = @"Payment";
}

- (void)viewWillAppear:(BOOL)animated{
    [self.webView loadHTMLString:strHtml baseURL:nil];
    [SVProgressHUD show];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [SVProgressHUD dismiss];
}

@end
