//
//  ObjUser.m
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ObjUser.h"
#import "Utility.h"
#import "Profile.h"
#import "Account_Type.h"
#import "Tokens.h"
@implementation ObjUser
@synthesize user;
+ (ObjUser*)sharedClient{
    static ObjUser *_sharedClient = nil;
    _sharedClient = [[ObjUser alloc] init];
    if(_sharedClient) {
        //_sharedClient = [[DBManager sharedClient] getConfig];
    }
    return _sharedClient;
}
+ (ObjUser *) setUserSaveAndUpdateFromDic:(NSDictionary *)dic{
    ObjUser * objUser = nil;
    if ([dic count]) {
        objUser = [[ObjUser alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * username = [dic objectForKey:@"username"];
        NSString * first_name = [dic objectForKey:@"first_name"];
        NSString * last_name = [dic objectForKey:@"last_name"];
        NSString * email = [dic objectForKey:@"email"];
        NSDictionary * dicProfile = [dic objectForKey:@"profile"];
        
        
        
    
        
        NSDictionary * dicTokens = [dic objectForKey:@"tokens"];
        NSInteger token_id = [[dicTokens objectForKey:@"id"]integerValue];
        NSString * strTokenId =[NSString stringWithFormat:@"%d",token_id];
        NSInteger credit_qty = [[dicTokens objectForKey:@"credit_qty"] integerValue];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:username shouldCleanWhiteSpace:YES]) username = @"";
        if ([Utility stringIsEmpty:first_name shouldCleanWhiteSpace:YES]) first_name = @"";
        if ([Utility stringIsEmpty:last_name shouldCleanWhiteSpace:YES]) last_name = @"";
        if ([Utility stringIsEmpty:email shouldCleanWhiteSpace:YES]) email = @"";
        NSString * account_type = @"";
        NSString * account_expiry = @"";
        NSDictionary * dicAccount = nil;
        if ([dic objectForKey:@"account"] != [NSNull null]) {
            dicAccount = [dic objectForKey:@"account"];
            account_type = [dicAccount objectForKey:@"account_type"];
            account_expiry = [dicAccount objectForKey:@"account_expiry"];
        }
        
        
        
        //if ([Utility stringIsEmpty:token_id shouldCleanWhiteSpace:YES]) token_id = @"";
        NSLog(@"acount type %@",account_type);
        
        User * obj = [User MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.username = username;
            obj.first_name = first_name;
            obj.last_name = last_name;
            obj.email = email;
            
            obj.profile = [ObjUser saveAndUpdateProfile:dicProfile];
            obj.profile.user_id = strUniId;
            obj.account_type = [ObjUser saveAndUpdateAccount:dicAccount];
            obj.account_type.user_id = strUniId;
            
            obj.tokens = [ObjUser saveAndUpdateToken:dicTokens];
            obj.tokens.user_id = strUniId;
            
            NSLog(@"profile ph %@",obj.profile.cell_phone);
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
            objUser.user = obj;
            
        }
        else{
            User * obj2 = [User MR_createEntity];
            obj2.idx = strUniId;
            obj2.username = username;
            obj2.first_name = first_name;
            obj2.last_name = last_name;
            obj2.email = email;
            obj2.profile = [ObjUser saveAndUpdateProfile:dicProfile];
            obj2.profile.user_id = strUniId;
            obj2.account_type = [ObjUser saveAndUpdateAccount:dicAccount];
            obj2.account_type.user_id = strUniId;
            
            obj2.tokens = [ObjUser saveAndUpdateToken:dicTokens];
            obj2.tokens.user_id = strUniId;
            NSLog(@"profile ph %@",obj2.profile.cell_phone);
            
            [obj2 MR_inContext:localContext];
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"User saved");
            }];
            objUser.user = obj2;
        }
    }
    return objUser;
}

+ (Profile *)saveAndUpdateProfile:(NSDictionary *)dic{
    NSDictionary * dicProfile = dic;
    NSInteger profileIdx = [[dicProfile objectForKey:@"id"] integerValue];
    NSString * strProfileId =[NSString stringWithFormat:@"%d",profileIdx];
    NSString * country_prefix = [dicProfile objectForKey:@"country_prefix"];
    //NSString * cell_phone = [dicProfile objectForKey:@"cell_phone"];
    NSString * address_1 = [dicProfile objectForKey:@"address_1"];
    NSString * address_2 = [dicProfile objectForKey:@"address_2"];
    NSString * city = [dicProfile objectForKey:@"city"];
    NSString * region_state = [dicProfile objectForKey:@"region_state"];
    
    NSString * country = [dicProfile objectForKey:@"country"];
    
    if ([Utility stringIsEmpty:strProfileId shouldCleanWhiteSpace:YES]) strProfileId = @"";
    if ([Utility stringIsEmpty:country_prefix shouldCleanWhiteSpace:YES]) country_prefix = @"";
    //if ([Utility stringIsEmpty:cell_phone shouldCleanWhiteSpace:YES]) cell_phone = @"";
    if ([Utility stringIsEmpty:address_1 shouldCleanWhiteSpace:YES]) address_1 = @"";
    if ([Utility stringIsEmpty:address_2 shouldCleanWhiteSpace:YES]) address_2 = @"";
    if ([Utility stringIsEmpty:city shouldCleanWhiteSpace:YES]) city = @"";
    if ([Utility stringIsEmpty:region_state shouldCleanWhiteSpace:YES]) region_state = @"";
    if ([Utility stringIsEmpty:country shouldCleanWhiteSpace:YES]) country = @"";
    NSInteger cell_phone = 0;
    if ([dicProfile objectForKey:@"cell_phone"] != [NSNull null]) {
        cell_phone = [[dicProfile objectForKey:@"cell_phone"] integerValue];
    }
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strProfileId];
    Profile * obj = [Profile MR_findFirstWithPredicate:predicate inContext:localContext];
    if (obj!= nil) {
        obj.country = country;
        //obj2.profile.cell_phone = cell_phone;
        obj.address_1 = address_1;
        obj.address_2 = address_2;
        obj.city = city;
        obj.region_state = region_state;
        obj.country = country;
        obj.user_id = strProfileId;
        obj.country_prefix = country_prefix;
    
        obj.cell_phone = [NSString stringWithFormat:@"%d",cell_phone];
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
        }];
        return obj;
    }
    else{
        Profile * obj2 = [Profile MR_createEntity];
        obj2.country = country;
        //obj2.profile.cell_phone = cell_phone;
        obj2.address_1 = address_1;
        obj2.address_2 = address_2;
        obj2.city = city;
        obj2.region_state = region_state;
        obj2.country = country;
        obj2.user_id = strProfileId;
        obj2.cell_phone = [NSString stringWithFormat:@"%d",cell_phone];
        obj2.country_prefix = country_prefix;
        
        [obj2 MR_inContext:localContext];
        // Save the modification in the local context
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            NSLog(@"User saved");
        }];
        return obj2;
    }
    return nil;
}

+ (Account_Type *)saveAndUpdateAccount:(NSDictionary *)dic{
    NSString * account_type = @"";
    NSString * account_expiry = @"";
    if ([dic count]) {
        NSDictionary * dicAccount = dic;
        account_type = [dicAccount objectForKey:@"account_type"];
        account_expiry = [dicAccount objectForKey:@"account_expiry"];
        
        if ([Utility stringIsEmpty:account_type shouldCleanWhiteSpace:YES])
        {
            return nil;
            account_type = @"";
        }
        else [Account_Type MR_truncateAll];
        
        if ([Utility stringIsEmpty:account_expiry shouldCleanWhiteSpace:YES]) account_expiry = @"";
    }
    
    Account_Type * obj =[Account_Type MR_createEntity];
    obj.account_type = account_type;
    obj.account_expiry = account_expiry;
    
    return obj;
}

+ (Tokens *)saveAndUpdateToken:(NSDictionary *)dic{
    NSDictionary * dicTokens = dic;
    NSInteger token_id = [[dicTokens objectForKey:@"id"]integerValue];
    NSString * strTokenId =[NSString stringWithFormat:@"%d",token_id];
    NSInteger credit_qty = [[dicTokens objectForKey:@"credit_qty"] integerValue];
    
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strTokenId];
    Tokens * obj = [Profile MR_findFirstWithPredicate:predicate inContext:localContext];
    if (obj!= nil) {
        obj.idx = strTokenId;
        obj.qty = [NSNumber numberWithInteger:credit_qty];
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
        }];
        return obj;
    }
    else{
        Tokens * obj2 = [Tokens MR_createEntity];
        obj2.idx = strTokenId;
        obj2.qty = [NSNumber numberWithInteger:credit_qty];
        
        [obj2 MR_inContext:localContext];
        // Save the modification in the local context
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            NSLog(@"User saved");
        }];
        return obj2;
    }
    return nil;
}

+ (NSString *) setUserFullName:(ObjUser *)obj{
    return [NSString stringWithFormat:@"%@ %@",obj.user.first_name,obj.user.last_name];
}

+ (NSAttributedString *) setAccountName:(ObjUser *)obj{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"account_type_icon"];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *normalString = [[NSAttributedString alloc] initWithString: [NSString stringWithFormat:@"  Account Type : %@",obj.user.account_type.account_type]];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] init];
    [myString appendAttributedString:attachmentString];
    [myString appendAttributedString:normalString];
    return myString;
}

+ (NSString *) setToken:(ObjUser *)obj{
    return [NSString stringWithFormat:@"Token : %d",[obj.user.tokens.qty integerValue]];
}
@end
