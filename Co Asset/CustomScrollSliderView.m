//
//  CustomScrollSliderView.m
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "CustomScrollSliderView.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"
@implementation CustomScrollSliderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect newScrollFrame = frame;
        newScrollFrame.origin.x = 0;
        newScrollFrame.origin.y = 0;
        scrollView = [[UIScrollView alloc] initWithFrame:newScrollFrame];
        [self addSubview:scrollView];
        
//        infinitePageView = [[InfinitePagingView alloc] initWithFrame:newScrollFrame];
//        infinitePageView.delegate = self;
//        [self addSubview:infinitePageView];
        
        CGRect newFrame = frame;
        newFrame.origin.x = 0;
        if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
            newFrame.origin.y = frame.size.height-30;
        }
        else{
            newFrame.origin.y = frame.size.height-40;
        }
        newFrame.size.height = 30;
        pageControl = [[UIPageControl alloc] initWithFrame:newFrame];
        [self addSubview:pageControl];
        
        NSLog(@"custom scroll initial!!");
    }
    return self;
}

- (void) loadImageViewWith:(NSArray *)arr{
    
    if (arr != nil) {
        NSLog(@"slider count %d",[arr count]);
        int count=[arr count];
        totalCount = count;
        arrContentView = (NSMutableArray *)arr;
        
        for (UIView * v in scrollView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                [v removeFromSuperview];
                //[v release];
            }
        }
        scrollView.pagingEnabled=TRUE;
        scrollView.bounces = NO;
        pageControl.numberOfPages=count;
        pageControl.currentPage=0;
        [pageControl setPageIndicatorTintColor:[UIColor grayColor]];
        [pageControl setCurrentPageIndicatorTintColor:[UIColor redColor]];
        /*if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
             [pageControl setCurrentPageIndicatorTintColor:[UIColor grayColor]];
        }*/
        
        NSString * strStartUrl = [arrContentView objectAtIndex:0];
        NSString * strEndUrl = [arrContentView objectAtIndex:totalCount-1];
        NSMutableArray * arrTemp = [NSMutableArray arrayWithArray:arr];
        [arrContentView removeAllObjects];
        [arrContentView addObject:strEndUrl];
        for (NSString * str in arrTemp) {
            [arrContentView addObject:str];
        }
        [arrContentView addObject:strStartUrl];
        
        totalCount = [arrContentView count];
        currentPagingIndex=1;
        pageControlUsed=TRUE;
        scrollViewMode2=ScrollViewModePaging2;
        scrollView.delegate=self;
        scrollView.showsHorizontalScrollIndicator=FALSE;
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * [arrContentView count], scrollView.frame.size.height)];
        int i = 0;
        for (NSString * strURL in arrContentView) {
            /*UIButton * btnMovieDetail=[[UIButton alloc]init];
            [btnMovieDetail setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
            //[btnMovieDetail setTag:page];
            [btnMovieDetail setTag:i];
            [btnMovieDetail addTarget:self action:@selector(clickPromo:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:btnMovieDetail];*/
            //pageControl.currentPage=i;
            UIImageView * detailImage=[[UIImageView alloc]init];
            detailImage.contentMode = UIViewContentModeScaleAspectFill;
            detailImage.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
            [detailImage setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
            [detailImage setImageWithURL:[NSURL URLWithString:strURL]];
            [detailImage setContentMode:UIViewContentModeScaleAspectFill];
            [scrollView addSubview:detailImage];
            
            //[infinitePageView addPageView:detailImage];
            i ++;
        }
        
        if(myTimer ==nil){
            myTimer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
            slidingIndex=currentPagingIndex;
        }
        [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.size.width,0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    }
}

- (void) scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
	//NSLog(@"scrollViewDidEndZooming");
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scroll {
    
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
    
    //[myTimer invalidate];
    pageControlUsed = FALSE;
    
    NSLog(@"scrollViewDidEndDecelerating total count %d and page index %d and contentOffset x:%f",totalCount,currentPagingIndex,scroll.contentOffset.x);
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scroll {
    
    CGFloat pageWidth = scroll.frame.size.width;
    int page = floor((scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    currentPagingIndex=page;
    
    
    NSInteger moveDirection = currentPagingIndex - _lastPageIndex;
    if (moveDirection == 0) {
        return;
    } else if (moveDirection > 0.f) {
        NSLog(@"right");
        pageControl.currentPage=currentPagingIndex-1;
        
    } else if (moveDirection < 0) {
        NSLog(@"left");
        pageControl.currentPage=currentPagingIndex-1;
    }
    _lastPageIndex = currentPagingIndex;
    
    if (currentPagingIndex+1 == totalCount) {
        NSLog(@"last index!!!");
        [scrollView scrollRectToVisible:CGRectMake(320,0,320,480) animated:NO];
        pageControl.currentPage=0;
    }
    else if (currentPagingIndex == 0){
        [scrollView scrollRectToVisible:CGRectMake(960+320,0,320,480) animated:NO];
        pageControl.currentPage=totalCount-2;
    }
    
    NSLog(@"scrollViewDidEndDecelerating total count %d and page index %d and contentOffset x:%f",totalCount,currentPagingIndex,scroll.contentOffset.x);
    
    if(myTimer==nil){
        myTimer=[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
        slidingIndex=currentPagingIndex;
        //[scroll scrollRectToVisible:CGRectMake(slidingIndex*scroll.frame.size.width, scroll.frame.origin.y, scroll.frame.size.width, scroll.frame.size.height) animated:YES];
    }
}

- (void)scrollToDirection:(NSInteger)moveDirection animated:(BOOL)animated
{
    CGRect adjustScrollRect;
    
        //if (0 != fmodf(scrollView.contentOffset.x, scrollView.frame.size.width)) return ;
        adjustScrollRect = CGRectMake(scrollView.contentOffset.x - scrollView.frame.size.width * moveDirection,
                                      scrollView.contentOffset.y,
                                      scrollView.frame.size.width, scrollView.frame.size.height);
    NSLog(@"abjust scroll rect %d",moveDirection);
    [scrollView scrollRectToVisible:adjustScrollRect animated:animated];
}

- (void) slideTimely{
    currentPagingIndex = slidingIndex;
    slidingIndex++;
    
    //if(slidingIndex>[arrContentView count]-1)
        //slidingIndex=0;
    if(totalCount == slidingIndex +1)
        slidingIndex=1;
    else if(slidingIndex == 0)
        slidingIndex = totalCount-1;
    
    NSLog(@"slideTimely current index %d and total index %d",currentPagingIndex,totalCount);
    [scrollView scrollRectToVisible:CGRectMake(slidingIndex*scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    pageControl.currentPage = slidingIndex-1;
    
    //[self performSelector:@selector(scrollViewDidEndDecelerating:) withObject:scrollView afterDelay:0.5f];*/
}

- (void) clickPromo:(UIButton *)sender{
    
    NSLog(@" here is click!! ");
    /*ObjectSlide * objS = [delegate.db getPromoById:sender.tag];
    if (![self stringIsEmpty:objS.strPromoLink shouldCleanWhiteSpace:YES]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: objS.strPromoLink]];
    }*/
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
