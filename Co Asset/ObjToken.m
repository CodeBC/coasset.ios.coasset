//
//  ObjToken.m
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

/* 
 {
 product_code: "TOK-50"
 description: "50 Tokens Pack"
 price: 45
 currency: "SGD"
 }
 */

#import "ObjToken.h"

@implementation ObjToken
@synthesize strProductCode,strName,strCurrency,price;

+ (NSArray *)parseProductTokenWithArray:(NSArray *)arr{
    NSMutableArray * arrTemp = nil;
    if ([arr count]) {
        arrTemp = [[NSMutableArray alloc] init];
    }
    for (NSDictionary * dic in arr) {
        ObjToken * token = nil;
        if ([dic count]) {
            token = [[ObjToken alloc] init];
        }
        token.strProductCode = [dic objectForKey:@"product_code"];
        token.strName = [dic objectForKey:@"description"];
        if ([dic objectForKey:@"price"] != [NSNull null]) {
            token.price = [[dic objectForKey:@"price"] floatValue];
        }
        token.strCurrency = [dic objectForKey:@"currency"];
        [arrTemp addObject:token];
    }
    return (NSArray *)arrTemp;
}
@end
