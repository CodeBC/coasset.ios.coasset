//
//  LabelImageVideoView.m
//  Co Asset
//
//  Created by Zayar on 5/16/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LabelImageVideoView.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
#import "Utility.h"
@interface LabelImageVideoView()
@property (nonatomic,strong) UILabel * lblCap;
@property (nonatomic,strong) UIImageView * imgView;
@property (nonatomic,strong) UIButton * btnPlay;
@end
@implementation LabelImageVideoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (UILabel *) lblCap{
    if (!_lblCap) {
        _lblCap = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 20, self.frame.size.height - 10 - DEAL_DETAIL_IMAGE_VIDEO_HEIGHT - 10)];
        _lblCap.backgroundColor = [UIColor clearColor];
        _lblCap.textColor = [UIColor blackColor];
        _lblCap.font = [UIFont systemFontOfSize:14];
        _lblCap.numberOfLines = 0;
    }
    return _lblCap;
}

- (UIImageView *) imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(30, self.lblCap.frame.origin.y+self.lblCap.frame.size.height+10, DEAL_DETAIL_IMAGE_VIDEO_WIDTH, DEAL_DETAIL_IMAGE_VIDEO_HEIGHT)];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [Utility makeCornerRadius:_imgView andRadius:5];
    }
    return _imgView;
}

- (UIButton *) btnPlay{
    if (!_btnPlay) {
        _btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnPlay setFrame:CGRectMake(self.imgView.frame.origin.x + self.imgView.frame.size.width + 10,  self.lblCap.frame.origin.y+self.lblCap.frame.size.height+10,  DEAL_DETAIL_IMAGE_VIDEO_WIDTH, DEAL_DETAIL_IMAGE_VIDEO_HEIGHT)];
        [_btnPlay setBackgroundColor:[UIColor clearColor]];
        [_btnPlay addTarget:self action:@selector(onPlay:) forControlEvents:UIControlEventTouchUpInside];
        [_btnPlay setBackgroundColor:[UIColor colorWithHexString:@"d24a4a"]];
        //[_btnPlay setImage:[UIImage imageNamed:@"heart_icon"] forState:UIControlStateNormal];
        [Utility makeCornerRadius:_btnPlay andRadius:5];
    }
    return _btnPlay;
}

- (void)setUpViews{
    [self addSubview:self.lblCap];
    [self addSubview:self.imgView];
    [self addSubview:self.btnPlay];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) loadWithCapName:(NSString *)strCapName{
    self.lblCap.text = [NSString stringWithFormat:@"%@",strCapName];
}

- (void) setImageToImageView:(NSString *)strImageUrl{
    [self.imgView setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:[UIImage imageNamed:@"my_deal_bg"]];
}

- (void)onPlay:(UIButton *)sender{
    
}


@end
