//
//  NewsViewController.m
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//


#import "NewsViewController.h"
#import "NewsDetailViewController.h"
#import "NewsCollectionViewCell.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjNews.h"
@interface NewsViewController ()
{
    NSMutableArray * arrNews;
    NSArray *searchResults;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewNews;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionViewNews.dataSource = self;
    self.collectionViewNews.delegate = self;
    [self.collectionViewNews reloadData];
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"FILTER" style:UIBarButtonItemStylePlain target:self action:@selector(tappedFilter)];
    /*UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    searchBar.backgroundImage = [[UIImage alloc] init];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBar];*/
    [self setNavBarTitle:@"NEWS"];
    [self setNavMenuButton];
    
    self.searchBar.backgroundColor = [UIColor colorWithHexString:@"ea4c46"];
}

- (void) viewDidAppear:(BOOL)animated{
}

- (void) viewWillAppear:(BOOL)animated{
    [self syncNews];
    [self reloadTheViews];
}

- (void) reloadTheViews{
    arrNews = [ObjNews getNewsArray];
    NSLog(@"news array count: %d",[arrNews count]);
    [self.collectionViewNews reloadData];
}

- (void) tappedFilter{
    NSLog(@"Filter is tapped");
}

- (void) onMenu:(UIBarButtonItem *)sender{
    [[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrNews count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NewsCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"newsCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 5;
    [cell setupCell];
    [cell loadCellWithNews:[arrNews objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewsDetailViewController * NewsDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
    ObjNews * obj = [arrNews objectAtIndex:indexPath.row];
    NewsDetailVC.objNews = obj;
    [self.navigationController pushViewController:NewsDetailVC animated:YES];
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(250, 80);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 50, 15);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) syncNews{
    [SVProgressHUD show];
    [[CoAssetAPIClient sharedClient] POST:[NSString stringWithFormat:@"%@",NEWS_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncNews successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arr = (NSArray *)myDict;
        NSLog(@"syncChurch count %d",[arr count]);
        for(NSInteger i=0;i<[arr count];i++){
            [ObjNews setNewsSaveAndUpdateFromDic:[arr objectAtIndex:i]];
        }
        [SVProgressHUD dismiss];
        [self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    ObjNews * obj = [searchResults objectAtIndex:indexPath.row];
    
    // Configure the cell
    [[cell textLabel] setText:[obj.news title]];
    cell.textLabel.font = [UIFont systemFontOfSize:13];

    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (homeDetailViewController == nil) {
     homeDetailViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"detailViewController"];
     }
     Poems * obj = [arrPoems objectAtIndex:indexPath.row];
     homeDetailViewController.objPoem = obj;
     [self.navigationController pushViewController:homeDetailViewController animated:YES];
     //[self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
    NewsDetailViewController * NewsDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
    ObjNews * obj = [searchResults objectAtIndex:indexPath.row];
    NewsDetailVC.objNews = obj;
    [self.navigationController pushViewController:NewsDetailVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.news.title contains[c] %@", searchText];
    searchResults = [arrNews filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self syncSearchWithWord:searchBar.text];
}

- (void) syncSearchWithWord:(NSString *)str{
    [SVProgressHUD show];
    [[CoAssetAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@%@/",NEWS_FILTER_LINK,str]] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncSearchWithWord successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arr = (NSArray *)myDict;
        //NSLog(@"syncChurch count %d",[arr count]);
        /*for(NSInteger i=0;i<[arr count];i++){
            [ObjNews setNewsSaveAndUpdateFromDic:[arr objectAtIndex:i]];
        }*/
        searchResults = [ObjNews getParseArray:arr];
        [self.searchDisplayController.searchResultsTableView reloadData];
        [SVProgressHUD dismiss];
        [self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) endSearchBarEditing{
    NSLog(@"endSearchBarEditing end editing!");
    self.searchDisplayController.active = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    [searchBar resignFirstResponder];
}



@end
