//
//  Tokens.h
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tokens : NSManagedObject

@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSNumber * qty;

@end
