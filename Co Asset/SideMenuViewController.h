//
//  SideMenuViewController.h
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : ADSlidingViewController
- (void)slidingViewControllerWillShowLeftView:(ADSlidingViewController *)slidingViewController;
- (void) setRouteToViewControllerId:(NSInteger )index;
@end
