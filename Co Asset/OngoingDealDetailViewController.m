//
//  OngoingDealDetailViewController.m
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "OngoingDealDetailViewController.h"
#import "DealDetailImageView.h"
#import "LabelAndValueView.h"
#import "LabelAndIconView.h"
#import "LabelDescriptionView.h"
#import "MultiLineLabelView.h"
#import "Utility.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "WebDescriptionView.h"
#import "ObjAuth.h"
#import "Deal.h"
@interface OngoingDealDetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) DealDetailImageView * dealDetailImageView;
@property (nonatomic, strong) LabelAndValueView * lblAndValueView;
@property (nonatomic, strong) LabelAndValueView * lblAndValueParticipantView;
@property (nonatomic, strong) LabelAndIconView * lblandIconView;
@property (nonatomic, strong) LabelDescriptionView * lblDescriptionView;
@property (nonatomic, strong) MultiLineLabelView * multLineView;
@property (nonatomic, strong) WebDescriptionView * webDescripView;
@property (nonatomic, strong) UIButton * btnTellMeMore;
@property (nonatomic, strong) UIButton * btnSubscribe;
@end

@implementation OngoingDealDetailViewController
@synthesize objDeal,is_from,strDealId;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.scrollView addSubview:self.dealDetailImageView];
    [self.scrollView addSubview:self.lblAndValueView];
    [self.scrollView addSubview:self.lblandIconView];
    [self.scrollView addSubview:self.lblDescriptionView];
    [self.scrollView addSubview:self.lblAndValueParticipantView];
    [self.scrollView addSubview:self.multLineView];
    [self.scrollView addSubview:self.webDescripView];
    [self.scrollView addSubview:self.btnTellMeMore];
    [self.scrollView addSubview:self.btnSubscribe];
    
    [self setNavBarTitle:@"DEAL DETAILS"];
}

- (DealDetailImageView *)dealDetailImageView{
    if (!_dealDetailImageView) {
        _dealDetailImageView = [[DealDetailImageView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, 196)];
        [Utility makeCornerRadius:_dealDetailImageView andRadius:5];
    }
    return _dealDetailImageView;
}

- (LabelAndValueView *) lblAndValueView{
    if (!_lblAndValueView) {
        _lblAndValueView = [[LabelAndValueView alloc] initWithFrame:CGRectMake(0, self.dealDetailImageView.frame.origin.y + self.dealDetailImageView.frame.size.height + 10, self.scrollView.frame.size.width, 40)];
        [Utility makeCornerRadius:_lblAndValueView andRadius:5];
    }
    return _lblAndValueView;
}

- (LabelAndIconView *) lblandIconView{
    if (!_lblandIconView) {
        _lblandIconView = [[LabelAndIconView alloc] initWithFrame:CGRectMake(0, self.lblAndValueView.frame.origin.y + self.lblAndValueView.frame.size.height + 10, self.scrollView.frame.size.width, 40)];
        [Utility makeCornerRadius:_lblandIconView andRadius:5];
    }
    return _lblandIconView;
}

- (LabelDescriptionView *) lblDescriptionView{
    if (!_lblDescriptionView) {
        _lblDescriptionView = [[LabelDescriptionView alloc] initWithFrame:CGRectMake(0, self.lblandIconView.frame.origin.y + self.lblandIconView.frame.size.height + 10, self.scrollView.frame.size.width, 70)];
        [Utility makeCornerRadius:_lblDescriptionView andRadius:5];
    }
    return _lblDescriptionView;
}

- (LabelAndValueView *) lblAndValueParticipantView{
    if (!_lblAndValueParticipantView) {
        _lblAndValueParticipantView = [[LabelAndValueView alloc] initWithFrame:CGRectMake(0, self.lblDescriptionView.frame.origin.y + self.lblDescriptionView.frame.size.height + 10, self.scrollView.frame.size.width, 40)];
        [Utility makeCornerRadius:_lblAndValueParticipantView andRadius:5];
    }
    return _lblAndValueParticipantView;
}

- (MultiLineLabelView *) multLineView{
    if (!_multLineView) {
        _multLineView = [[MultiLineLabelView alloc] initWithFrame:CGRectMake(0, self.lblAndValueParticipantView.frame.origin.y + self.lblAndValueParticipantView.frame.size.height + 10, self.scrollView.frame.size.width, 107)];
        [Utility makeCornerRadius:_multLineView andRadius:5];
    }
    return _multLineView;
}

- (WebDescriptionView *) webDescripView{
    if (!_webDescripView) {
        _webDescripView = [[WebDescriptionView alloc] initWithFrame:CGRectMake(0, self.multLineView.frame.origin.y + self.multLineView.frame.size.height + 10, self.scrollView.frame.size.width, 107)];
        //[Utility makeCornerRadius:_webDescripView andRadius:5];
        _webDescripView.owner = self;
    }
    return _webDescripView;
}

- (UIButton *) btnTellMeMore{
    if (!_btnTellMeMore) {
        _btnTellMeMore = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnTellMeMore.frame = CGRectMake(0, self.btnSubscribe.frame.origin.y + self.btnSubscribe.frame.size.height + 10, self.scrollView.frame.size.width, 40);
        [Utility makeCornerRadius:_btnTellMeMore andRadius:5];
        [_btnTellMeMore setBackgroundColor:[UIColor colorWithHexString:@"d24a4a"]];
        _btnTellMeMore.titleLabel.textColor = [UIColor whiteColor];
        [_btnTellMeMore addTarget:self action:@selector(onTellMeMore:) forControlEvents:UIControlEventTouchUpInside];
        [_btnTellMeMore setTitle:@"Tell Me More" forState:UIControlStateNormal];
    }
    return _btnTellMeMore;
}

- (UIButton *) btnSubscribe{
    if (!_btnSubscribe) {
        _btnSubscribe = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnSubscribe.frame = CGRectMake(0, self.webDescripView.frame.origin.y + self.webDescripView.frame.size.height + 10, self.scrollView.frame.size.width, 40);
        [Utility makeCornerRadius:_btnSubscribe andRadius:5];
        [_btnSubscribe setBackgroundColor:[UIColor colorWithHexString:@"FED037"]];
        _btnSubscribe.titleLabel.textColor = [UIColor whiteColor];
        [_btnSubscribe addTarget:self action:@selector(onSubcribe:) forControlEvents:UIControlEventTouchUpInside];
        [_btnSubscribe setTitle:@"Subscribe" forState:UIControlStateNormal];
    }
    return _btnSubscribe;
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"obj unid %@",strDealId);
    NSLog(@"obj deal min investment %d",[objDeal.deal.min_investment integerValue]);
    [self syncOngoingDealDetail:strDealId];
    [self viewLoadWith:objDeal];
}

- (void)viewLoadWith:(ObjDeal *)obj{
    self.lblTitle.text = obj.deal.offer_title;
    [self.dealDetailImageView loadTheViewWithDeal:obj];
    [self.lblAndValueView loadWithCapName:@"Project Type" andName:obj.project.project_type];
    [self.lblandIconView loadWithCapName:@"Address" andName:obj.project.address_1 andIconName:@"location-pin_black"];
    [self.lblDescriptionView loadWithCapName:obj.deal.short_description];
    [self.lblAndValueParticipantView loadWithCapName:@"Fancy (Participate)" andName:[NSString stringWithFormat:@"%d",[obj.project.developer integerValue]]];
    [self.multLineView setLine1Name:@"Currency" andName:obj.deal.currency];
    [self.multLineView setLine2Name:@"Min. Amt" andName:[NSString stringWithFormat:@"%d",[obj.deal.min_investment integerValue]] andCurrencyCode:obj.deal.currency];
    [self.multLineView setLine3Name:@"Horizon (mths)" andName:[NSString stringWithFormat:@"%d",[obj.deal.time_horizon integerValue]]];
    [self.multLineView setLine4Name:@"Annualized Return (%)" andName:[NSString stringWithFormat:@"%.2f",[obj.deal.max_annual_return floatValue]]];
    
    [self.webDescripView loadWithWebString:obj.deal.long_description];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.btnTellMeMore.frame.origin.y+self.btnTellMeMore.frame.size.height)];
    
    
    /*if (is_from == PROJECT_ALIVE) {
        self.btnSubscribe.hidden = NO;
        self.btnTellMeMore.hidden = NO;
        NSLog(@"PROJECT_ALIVE");
        
    }
    else if(is_from == PROJECT_CLOSED){
        self.btnSubscribe.hidden = YES;
        self.btnTellMeMore.hidden = YES;
        
        NSLog(@"PROJECT_CLOSED");
    }*/
}

- (void) webDescriptionDidFinish:(WebDescriptionView *) webDescriptionView andWebHeight:(CGSize)webSize{
    CGRect webDescriptionViewFrame = self.webDescripView.frame;
    webDescriptionViewFrame.size.height = webSize.height;
    self.webDescripView.frame = webDescriptionViewFrame;
    [Utility makeCornerRadius:self.webDescripView andRadius:5];
    self.btnSubscribe.frame = CGRectMake(0, self.webDescripView.frame.origin.y + self.webDescripView.frame.size.height + 10, self.scrollView.frame.size.width, 40);
    self.btnTellMeMore.frame = CGRectMake(0, self.btnSubscribe.frame.origin.y + self.btnSubscribe.frame.size.height + 10, self.scrollView.frame.size.width, 40);
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.btnTellMeMore.frame.origin.y+self.btnTellMeMore.frame.size.height)];
    /*if(is_from == PROJECT_CLOSED){
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.webDescripView.frame.origin.y+self.webDescripView.frame.size.height)];
    }*/
}

- (void) onTellMeMore:(UIButton *)sender{
    [self syncTellMeMore:objDeal];
}

- (void) syncTellMeMore:(ObjDeal *)obj{
    [SVProgressHUD showWithStatus:@"Email sending to CoAsset.."];
    ObjAuth * objAuth = [ObjAuth getCurrentAuth];
    
    NSLog(@"token %@, token type %@",objAuth.access_token,objAuth.token_type);
    NSLog(@"deal id %@",obj.deal.idx);
    [[CoAssetAPIClient sharedClientWithHeader:objAuth andLink:@""] POST:[NSString stringWithFormat:@"%@%@/",TELL_ME_MORE_LINK,obj.deal.idx] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncTellMeMore successfully return!!! %@",json);
        
        [SVProgressHUD showSuccessWithStatus:@"Email sent to CoAsset."];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
        [SVProgressHUD showSuccessWithStatus:@"Email sent to CoAsset."];
    }];
}

- (void) syncSubscribe:(ObjDeal *)obj{
    [SVProgressHUD show];
    ObjAuth * objAuth = [ObjAuth getCurrentAuth];
    NSLog(@"token %@, token type %@",objAuth.access_token,objAuth.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:objAuth andLink:@""] POST:[NSString stringWithFormat:@"%@%@/",SUBSCIBE_LINK,obj.deal.idx] parameters:nil success:^(AFHTTPRequestOperation *operation, id json){
        NSLog(@"syncSubscribe successfully return!!! %@",json);
        NSString * strMsg = [json objectForKey:@"success"];
        [SVProgressHUD showSuccessWithStatus:strMsg];
        //[SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) onSubcribe:(UIButton *)sender{
    [self syncSubscribe:objDeal];
}

- (void) syncOngoingDealDetail:(NSString *)strId{
    
    [SVProgressHUD show];
    [[CoAssetAPIClient sharedClient] POST:[NSString stringWithFormat:@"%@/%@",ONGOING_DEAL_LINK,strId] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncNews successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        objDeal = [ObjDeal setDealDetailSaveAndUpdateFromDic:myDict];
        [self viewLoadWith:objDeal];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [ObjDeal deleteDeals];
}

@end
