//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable
NSString * const APP_DB = @"coasset.sqlite";
NSString * const APP_TITLE = @"CoAssets";
NSString * const APP_ID = @"---";
NSString * const APP_BASED_LINK = @"https://coassets.com/";
NSString * const NEWS_LINK = @"api/news";
NSString * const NEWS_FILTER_LINK = @"/api/news/filter/";
NSString * const SIGN_UP_LINK = @"/api/profile/register/";
NSString * const ONGOING_DEAL_LINK = @"api/offers";
NSString * const CLOSED_DEAL_LINK = @"api/offers/closed/";
NSString * const PROFILE_LINK = @"https://coassets.com/api/profile";
NSString * const PROJECT_LINK = @"https://coassets.com/api/profile/projects/";
NSString * const OWNED_PROJECT_LINK = @"https://coassets.com/api/profile/own_projects/";
NSString * const SIGN_IN_LINK = @"https://coassets.com/oauth2/access_token/";
NSString * const CHANGE_PASS_LINK = @"https://coassets.com/api/change-password/";
NSString * const TELL_ME_MORE_LINK = @"https://coassets.com/api/offers/tellmemore/";
NSString * const SUBSCIBE_LINK = @"https://coassets.com/api/offers/subscribe/";
NSString * const TOKENS_LINK = @"https://coassets.com/api/product/tokens/";
NSString * const TOKENS_PAYPAL_LINK = @"https://coassets.com/api/paypal/payment/";
///api/offers/subscribe/<
NSString * const PERSONAL_INFO_LINK = @"https://coassets.com/api/profile/";
NSString * const CLIENT_ID = @"6c85f05b29e609bcacfd";
NSString * const CLIENT_SECRET = @"207219bcc24a06f112011fc3574f389b71339121";

CGFloat const DEAL_CATE_CELL_HEIGHT = 144;
CGFloat const DEAL_CELL_HEIGHT = 190;
CGFloat const DEAL_IMAGE_LABEL_HEIGHT = 44;
CGFloat const DEAL_DETAIL_IMAGE_VIDEO_HEIGHT = 70;
CGFloat const DEAL_DETAIL_IMAGE_VIDEO_WIDTH = 92;

CGFloat const DEAL_IMAGE_CATE_LABEL_WIDTH = 100;
CGFloat const DEAL_DETAIL_SCROLL_VIEW_OFFSET = 10;

CGFloat const PROJECT_CATE_CELL_HEIGHT = 196;
CGFloat const PROJECT_CATE_CELL_WIDTH = 248;

CGFloat const PROJECT_LIST_CELL_HEIGHT = 50;
CGFloat const PROJECT_LIST_CELL_WIDTH = 255;

NSInteger const PROFILE_VIEW_CONTROLLER = 0;
NSInteger const DEAL_VIEW_CONTROLLER = 1;
NSInteger const NEWS_VIEW_CONTROLLER = 2;
NSInteger const SIGNIN_VIEW_CONTROLLER = 3;

NSInteger const PROJECT_ALIVE = 0;
NSInteger const PROJECT_CLOSED = 1;
@end
