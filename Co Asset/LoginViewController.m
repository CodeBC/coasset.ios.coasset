//
//  LoginViewController.m
//  Co Asset
//
//  Created by ark on 9/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LoginViewController.h"
#import "NewsViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjAuth.h"
#import "SideMenuViewController.h"
@interface LoginViewController ()
{
}

@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)tappedDeals:(id)sender {
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self textFieldSetup];
    
    //[self setNavBarTitle:@"LOGIN"];
    [self setNavMenuButton];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"ea4c46"]];
}

- (void) onMenu:(UIBarButtonItem *)sender{
    [[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void) viewWillAppear:(BOOL)animated{
    //[self navbarSetup];
}

- (void) navbarSetup{
    [[self navigationController] setNavigationBarHidden:YES];
}

- (void) setupView
{
    self.viewBackground.layer.cornerRadius = 5;
    self.btnLogin.layer.cornerRadius = 5;
}

- (void) textFieldSetup{
    
    self.txtUsername.delegate = self;
    self.txtPassword.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtUsername) {
        [self.txtUsername resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    } else if (textField == self.txtPassword) {
        [self.txtPassword resignFirstResponder];
    }
    return YES;
}

- (IBAction)tappedLogin:(id)sender {
    if ([[self.txtUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"Please enter your email"];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtUsername];
        return;
    }
    if ([[self.txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"Please enter your password"];
        [self textFieldDidEndEditing:self.txtPassword];
        return;
    }
    
    [self syncLogin:self.txtUsername.text andPass:self.txtPassword.text];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)syncLogin:(NSString *)strName andPass:(NSString *)strPass1{
    [SVProgressHUD show];
    NSLog(@"str name %@ and pass %@",strName,strPass1);
    NSDictionary * params = @{@"client_id":CLIENT_ID,@"client_secret":CLIENT_SECRET,@"grant_type":@"password",@"username":strName,@"password":strPass1};
    [[CoAssetAPIClient sharedClientWithURL:SIGN_IN_LINK] POST:[NSString stringWithFormat:@"%@",@""] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncLogin successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        NSString * access_token = [myDict objectForKey:@"access_token"];
        NSString * refresh_token = [myDict objectForKey:@"refresh_token"];
        NSString * scope = [myDict objectForKey:@"scope"];
        NSString * token_type = [myDict objectForKey:@"token_type"];
        NSInteger expires = [[myDict objectForKey:@"expires_in"] integerValue];
        ObjAuth * objAuth = [[ObjAuth alloc] init];
        objAuth.access_token = access_token;
        objAuth.refresh_token = refresh_token;
        objAuth.scope = scope;
        objAuth.token_type = token_type;
        objAuth.expires_timetick = expires;
        objAuth.password = strPass1;
        [ObjAuth setCurrentAuth:objAuth];
        /*NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
        [prefs setObject:objAuth forKey:SIGN_IN_LINK];
        [prefs synchronize];*/
        SideMenuViewController * sideMenu = (SideMenuViewController *)[self slidingViewController].leftViewController;
        [sideMenu setRouteToViewControllerId:PROFILE_VIEW_CONTROLLER];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
