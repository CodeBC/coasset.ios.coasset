//
//  LoginViewController.h
//  Co Asset
//
//  Created by ark on 9/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : BasedViewController <UITextFieldDelegate>

@end
