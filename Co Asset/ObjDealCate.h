//
//  ObjDealCate.h
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjDealCate : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strImageName;
@end
