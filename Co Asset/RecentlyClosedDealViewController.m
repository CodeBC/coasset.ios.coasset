//
//  RecentlyClosedDealViewController.m
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "RecentlyClosedDealViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjDeal.h"
#import "RecentlyClosedDealDetailViewController.h"
#import "OngoingDealDetailViewController.h"
#import "DealOngoingTableViewCell.h"
@interface RecentlyClosedDealViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (nonatomic,strong) NSArray * arrList;
@property (nonatomic,strong) RecentlyClosedDealDetailViewController * detailViewController;
@property (nonatomic,strong) OngoingDealDetailViewController * ongoingDealDetailViewController;
@end

@implementation RecentlyClosedDealViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"RECENTLY CLOSED DEALS"];
    [self.tbl reloadData];
    [self.tbl setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncClosedDeal];
    //[self reloadTheViews];
}

- (void)reloadTheViews{
    self.arrList = [ObjDeal getDealArray];
    [self.tbl reloadData];
}

- (RecentlyClosedDealDetailViewController *) detailViewController{
    if (!_detailViewController) {
        _detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ClosedDealDetail"];
    }
    return _detailViewController;
}

- (OngoingDealDetailViewController *)ongoingDealDetailViewController{
    if (!_ongoingDealDetailViewController) {
        _ongoingDealDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OngoingDealDetail"];
    }
    return _ongoingDealDetailViewController;
}

- (void) syncClosedDeal{
    [SVProgressHUD show];
    [[CoAssetAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",CLOSED_DEAL_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncNews successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arr = (NSArray *)myDict;
    
        self.arrList = [ObjDeal parseDealsWithArray:arr];
        [self.tbl reloadData];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DealRecently";
	DealOngoingTableViewCell *cell = (DealOngoingTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[DealOngoingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    ObjDeal * obj = [self.arrList   objectAtIndex:indexPath.row];
    [cell viewLoadWithDealOngoing:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEAL_CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjDeal * obj = [self.arrList   objectAtIndex:indexPath.row];
    self.ongoingDealDetailViewController.objDeal = obj;
    self.ongoingDealDetailViewController.is_from = PROJECT_CLOSED;
    self.ongoingDealDetailViewController.strDealId = obj.deal.idx;
    [self.navigationController pushViewController:self.ongoingDealDetailViewController animated:YES];
}


@end
