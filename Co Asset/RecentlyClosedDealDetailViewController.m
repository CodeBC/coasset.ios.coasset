//
//  RecentlyClosedDealDetailViewController.m
//  Co Asset
//
//  Created by Zayar on 5/16/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "RecentlyClosedDealDetailViewController.h"
#import "DealDetailImageView.h"
#import "Utility.h"
#import "StringTable.h"
#import "LabelImageVideoView.h"
#import "LabelSingleView.h"
#import "MultiLineLabelView.h"
@interface RecentlyClosedDealDetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *lblTittle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) DealDetailImageView * dealDetailImageView;
@property (nonatomic, strong) LabelImageVideoView * labelImageVideoView;
@property (nonatomic, strong) UIButton * btnTellMeMore;
@property (nonatomic, strong) LabelSingleView * labelSingleView;
@property (nonatomic, strong) MultiLineLabelView * multLineView;
@end

@implementation RecentlyClosedDealDetailViewController
@synthesize objDeal;

- (DealDetailImageView *)dealDetailImageView{
    if (!_dealDetailImageView) {
        _dealDetailImageView = [[DealDetailImageView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, 196)];
        [Utility makeCornerRadius:_dealDetailImageView andRadius:5];
    }
    return _dealDetailImageView;
}

- (LabelImageVideoView *)labelImageVideoView{
    if (!_labelImageVideoView) {
        _labelImageVideoView = [[LabelImageVideoView alloc] initWithFrame:CGRectMake(0, self.dealDetailImageView.frame.origin.y + self.dealDetailImageView.frame.size.height + DEAL_DETAIL_SCROLL_VIEW_OFFSET, self.scrollView.frame.size.width, 150)];
        [Utility makeCornerRadius:_labelImageVideoView andRadius:5];
    }
    return _labelImageVideoView;
}

- (UIButton *) btnTellMeMore{
    if (!_btnTellMeMore) {
        _btnTellMeMore = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnTellMeMore.frame = CGRectMake(0, self.labelImageVideoView.frame.origin.y + self.labelImageVideoView.frame.size.height + DEAL_DETAIL_SCROLL_VIEW_OFFSET, self.scrollView.frame.size.width, 40);
        [Utility makeCornerRadius:_btnTellMeMore andRadius:5];
        [_btnTellMeMore setBackgroundColor:[UIColor colorWithHexString:@"d24a4a"]];
        _btnTellMeMore.titleLabel.textColor = [UIColor whiteColor];
        [_btnTellMeMore addTarget:self action:@selector(onTellMeMore:) forControlEvents:UIControlEventTouchUpInside];
        [_btnTellMeMore setTitle:@"Tell Me More" forState:UIControlStateNormal];
    }
    return _btnTellMeMore;
}

- (LabelSingleView *)labelSingleView{
    if (!_labelSingleView) {
        _labelSingleView = [[LabelSingleView alloc] initWithFrame:CGRectMake(0, self.btnTellMeMore.frame.origin.y + self.btnTellMeMore.frame.size.height + DEAL_DETAIL_SCROLL_VIEW_OFFSET, self.scrollView.frame.size.width, 40)];
        [Utility makeCornerRadius:_labelSingleView andRadius:5];
    }
    return _labelSingleView;
}

- (MultiLineLabelView *) multLineView{
    if (!_multLineView) {
        _multLineView = [[MultiLineLabelView alloc] initWithFrame:CGRectMake(0, self.labelSingleView.frame.origin.y + self.labelSingleView.frame.size.height + 10, self.scrollView.frame.size.width, 107)];
        [Utility makeCornerRadius:_multLineView andRadius:5];
    }
    return _multLineView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.scrollView addSubview:self.dealDetailImageView];
    [self.scrollView addSubview:self.labelImageVideoView];
    [self.scrollView addSubview:self.btnTellMeMore];
    [self.scrollView addSubview:self.labelSingleView];
    [self.scrollView addSubview:self.multLineView];
}

- (void)viewWillAppear:(BOOL)animated{
    //[self syncOngoingDealDetail:objDeal.deal.idx];
    [self viewLoadWith:objDeal];
}

- (void)viewLoadWith:(ObjDeal *)obj{
    self.lblTittle.text = obj.deal.offer_title;
    [self.dealDetailImageView loadTheViewWithDeal:obj];
    [self.labelImageVideoView loadWithCapName:obj.deal.short_description];
    [self.labelImageVideoView setImageToImageView:[NSString stringWithFormat:@"%@/%@",APP_BASED_LINK,obj.project.photo]];
    [self.labelSingleView loadWithCapName:obj.deal.offer_type];
    [self.multLineView setLine1Name:@"Currency" andName:obj.deal.currency];
    [self.multLineView setLine2Name:@"Min. Amt" andName:[NSString stringWithFormat:@"%.2f",[obj.deal.min_investment floatValue]]];
    [self.multLineView setLine3Name:@"Horizon (mths)" andName:[NSString stringWithFormat:@"%d",[obj.deal.time_horizon integerValue]]];
    [self.multLineView setLine4Name:@"Annualized Return (%)" andName:[NSString stringWithFormat:@"%d",[obj.deal.time_horizon integerValue]]];
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.multLineView.frame.origin.y+self.multLineView.frame.size.height)];
}

@end
