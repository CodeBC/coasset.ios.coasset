//
//  Profile.m
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Profile.h"


@implementation Profile

@dynamic idx;
@dynamic user_id;
@dynamic country_prefix;
@dynamic cell_phone;
@dynamic address_1;
@dynamic address_2;
@dynamic city;
@dynamic region_state;
@dynamic country;

@end
