//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}
extern NSString * const APP_DB;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;
extern NSString * const APP_BASED_LINK;
extern NSString * const NEWS_LINK;
extern NSString * const NEWS_FILTER_LINK;
extern NSString * const ONGOING_DEAL_LINK;
extern NSString * const CLOSED_DEAL_LINK;
extern NSString * const SIGN_IN_LINK;
extern NSString * const PROFILE_LINK;
extern NSString * const CHANGE_PASS_LINK;
extern NSString * const TELL_ME_MORE_LINK;
extern NSString * const SUBSCIBE_LINK;
extern NSString * const PERSONAL_INFO_LINK;
extern NSString * const PROJECT_LINK;
extern NSString * const TOKENS_LINK;
extern NSString * const TOKENS_PAYPAL_LINK;
extern NSString * const SIGN_UP_LINK ;

extern NSString * const CLIENT_ID;
extern NSString * const CLIENT_SECRET;

extern CGFloat const DEAL_CELL_HEIGHT;
extern CGFloat const DEAL_IMAGE_LABEL_HEIGHT;
extern CGFloat const DEAL_CATE_CELL_HEIGHT;
extern CGFloat const DEAL_IMAGE_CATE_LABEL_WIDTH;
extern CGFloat const PROJECT_CATE_CELL_HEIGHT;
extern CGFloat const PROJECT_CATE_CELL_WIDTH;
extern CGFloat const DEAL_DETAIL_IMAGE_VIDEO_HEIGHT;
extern CGFloat const DEAL_DETAIL_IMAGE_VIDEO_WIDTH;

extern CGFloat const DEAL_DETAIL_SCROLL_VIEW_OFFSET;

extern CGFloat const PROJECT_LIST_CELL_HEIGHT;
extern CGFloat const PROJECT_LIST_CELL_WIDTH;

extern NSInteger const PROFILE_VIEW_CONTROLLER ;
extern NSInteger const DEAL_VIEW_CONTROLLER ;
extern NSInteger const NEWS_VIEW_CONTROLLER ;
extern NSInteger const SIGNIN_VIEW_CONTROLLER ;

extern NSString * const OWNED_PROJECT_LINK;
extern NSInteger const PROJECT_ALIVE;
extern NSInteger const PROJECT_CLOSED;
@end
