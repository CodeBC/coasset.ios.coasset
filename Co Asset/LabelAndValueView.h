//
//  ProjectTypeView.h
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelAndValueView : UIView
- (void) loadWithCapName:(NSString *)strCapName andName:(NSString *)strName;

@end
