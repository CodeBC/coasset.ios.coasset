//
//  CoAssetAPIClient.h
//  Co Asset
//
//  Created by Zayar on 5/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "ObjAuth.h"
@interface CoAssetAPIClient : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;
+ (instancetype)sharedClientWithURL:(NSString *)strLink;
+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andLink:(NSString *)strLink;
+ (instancetype)sharedClientWithHeader:(ObjAuth *)obj andChPassLink:(NSString *)strLink;
@end
