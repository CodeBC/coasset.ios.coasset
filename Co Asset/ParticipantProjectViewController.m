//
//  ParticipantProjectViewController.m
//  Co Asset
//
//  Created by Zayar on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ParticipantProjectViewController.h"
#import "CoAssetAPIClient.h"
#import "StringTable.h"
#import "ObjProject.h"
#import "ProjectCollectionViewCell.h"
#import "OngoingDealDetailViewController.h"
#import "Utility.h"
#import "Account_Type.h"
#import "Tokens.h"
#import "ProjectCateCell.h"
#import "TokenViewController.h"
@interface ParticipantProjectViewController ()
{
    NSArray * arrProject;
}
@property (strong, nonatomic) IBOutlet UICollectionView *projectCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountType;
@property (strong, nonatomic) IBOutlet UILabel *lblToken;
@property (strong, nonatomic) NSMutableArray * arrList;
@property (nonatomic,strong) OngoingDealDetailViewController * dealDetailViewController;
@property (strong, nonatomic) TokenViewController * tokenViewController;
@end

@implementation ParticipantProjectViewController
@synthesize is_from,objUser;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.projectCollectionView setBackgroundColor:[UIColor clearColor]];
    [self setNavBarTitle:@"PROJECTS"];
    [self hardCodeMenuList];
}

- (TokenViewController *) tokenViewController{
    if (!_tokenViewController) {
        _tokenViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TokenViewController"];
    }
    return _tokenViewController;
}

- (NSMutableArray *)arrList{
    if (!_arrList) {
        _arrList = [[NSMutableArray alloc] init];
    }
    return _arrList;
}

- (void)hardCodeMenuList{
    ObjProjectCate * obj = [[ObjProjectCate alloc]init];
    obj.idx = 1;
    obj.strName = @"Your Participated Projects";
    obj.strImageName = @"own_prj_bg";
    obj.strDescription = @"find a project";
    [self.arrList addObject:obj];
    
    obj = [ObjProjectCate new];
    obj.idx = 2;
    obj.strName = @"Your Owned Projects";
    obj.strImageName = @"participant_bg";
    obj.strDescription = @"find a project";
    [self.arrList addObject:obj];
}

- (OngoingDealDetailViewController *)dealDetailViewController{
    if (!_dealDetailViewController) {
        _dealDetailViewController = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"OngoingDealDetail"];
    }
    return _dealDetailViewController;
}

- (IBAction)onToken:(UIButton *)sender {
    [self.navigationController pushViewController:self.tokenViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    if(is_from == 0)
    [self syncParticipantProject];
    
    if(is_from == 1)
    [self syncOwnedProject];
    
    [self setAccountTypeValue:objUser.user.account_type.account_type];
    [self setTokenValue:[NSString stringWithFormat:@"%d",[objUser.user.tokens.qty integerValue]]];
    
}

- (void)setAccountTypeValue:(NSString *)str{
    if ([Utility stringIsEmpty:str shouldCleanWhiteSpace:YES]) {
        str = @"-";
    }
    self.lblAccountType.text = [NSString stringWithFormat:@"Account Type: %@",str];
}

- (void)setTokenValue:(NSString *)str{
    if ([Utility stringIsEmpty:str shouldCleanWhiteSpace:YES]) {
        str = @"0";
    }
    self.lblToken.text = [NSString stringWithFormat:@"Token: %@",str];
}

- (void)syncOwnedProject{
    [SVProgressHUD show];
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    NSLog(@"token %@, token type %@",obj.access_token,obj.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:obj andLink:@""] GET:[NSString stringWithFormat:@"%@",OWNED_PROJECT_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncProfile successfully return!!! %@",json);
    
        NSArray * arrTemp = (NSArray *)json;
        arrProject = [ObjProject setProjectForListSaveAndUpdateFromDic:arrTemp];
        NSLog(@"arrProject count %d",[arrProject count]);
        [self.projectCollectionView reloadData];
        
        [SVProgressHUD dismiss];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncParticipantProject{
    [SVProgressHUD show];
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    NSLog(@"token %@, token type %@",obj.access_token,obj.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:obj andLink:@""] GET:[NSString stringWithFormat:@"%@",PROJECT_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncProfile successfully return!!! %@",json);
        
        NSArray * arrTemp = (NSArray *)json;
        arrProject = [ObjProject setProjectForListSaveAndUpdateFromDic:arrTemp];
        NSLog(@"arrProject count %d",[arrProject count]);
        [self.projectCollectionView reloadData];
        
        [SVProgressHUD dismiss];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (section == 0)
        return 1;
    else return [arrProject count];
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ProjectCateCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ProjectCateCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 5;
        [cell setupCell];
        if (is_from == 0)
            [cell loadCellWithObjCate:[self.arrList objectAtIndex:0]];
        else
            [cell loadCellWithObjCate:[self.arrList objectAtIndex:1]];
        
        return cell;
    }
    else {
        ProjectCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ProjectCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 5;
        [cell setupCell];
        [cell loadCellWithObjProject:[arrProject objectAtIndex:indexPath.row]];
        return cell;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ObjProject * objProject = [arrProject objectAtIndex:indexPath.row];
    ObjDeal * objDeal = [[ObjDeal alloc] init];
    objDeal.deal.idx = objProject.project.idx;
    self.dealDetailViewController.objDeal = objDeal;
    self.dealDetailViewController.strDealId= objDeal.deal.idx;
    [self.navigationController pushViewController:self.dealDetailViewController animated:YES];
    
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval;
    if (indexPath.section == 0)
       retval = CGSizeMake(PROJECT_CATE_CELL_WIDTH, PROJECT_CATE_CELL_HEIGHT);
    else retval = CGSizeMake(PROJECT_LIST_CELL_WIDTH, PROJECT_LIST_CELL_HEIGHT);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 70, 1, 70);
}

@end
