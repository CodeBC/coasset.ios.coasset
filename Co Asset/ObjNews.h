//
//  ObjNews.h
//  Co Asset
//
//  Created by Zayar on 5/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "News.h"
@interface ObjNews : NSObject
@property (nonatomic,strong) News * news;
+ (ObjNews *) setNewsSaveAndUpdateFromDic:(NSDictionary *)dic;
+ (ObjNews *) setNewsDetailSaveAndUpdateFromDic:(NSDictionary *)dic;
+ (NSMutableArray *) getNewsArray;
- (NSString *) getHumanDate;
+ (NSMutableArray *) getParseArray:(NSArray *)dicArrNews;
+ (ObjNews *) parseNews:(NSDictionary *)dic;
@end
