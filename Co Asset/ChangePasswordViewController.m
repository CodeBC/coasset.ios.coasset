//
//  ChangePasswordViewController.m
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Utility.h"
#import "StringTable.h"
#import "ObjAuth.h"
#import "CoAssetAPIClient.h"
@interface ChangePasswordViewController ()
{
    UITextField *activeField;
    IBOutlet UIScrollView *scrollView;
}
@property (strong, nonatomic) IBOutlet UIView *viewTextBg;
@property (strong, nonatomic) IBOutlet UITextField *txtOldPass;
@property (strong, nonatomic) IBOutlet UITextField *txtNewPass;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPass;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;

@end

@implementation ChangePasswordViewController
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"CHANGE PASSWORD"];
    [Utility makeCornerRadius:self.viewTextBg andRadius:5];
    [Utility makeCornerRadius:self.btnUpdate andRadius:5];
    [self textFieldSetup];
    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
}

- (void)viewWillAppear:(BOOL)animated{
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)onUpdate:(id)sender {
    [self validateTheFields];
}

- (void) textFieldSetup{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtOldPass) {
        [self.txtOldPass resignFirstResponder];
        [self.txtNewPass becomeFirstResponder];
    }else if (textField == self.txtNewPass){
        [self.txtNewPass resignFirstResponder];
        [self.txtConfirmPass becomeFirstResponder];
    }
    else if(textField == self.txtConfirmPass){
        [self.txtConfirmPass resignFirstResponder];
    }
    return YES;
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

- (void)validateTheFields{
    if ([[self.txtOldPass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"Please enter your old password."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtOldPass];
        return;
    }
    if ([[self.txtNewPass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"Please enter your new password."];
        self.txtNewPass.text = @"";
        [self textFieldDidEndEditing:self.txtNewPass];
        return;
    }
    ObjAuth * objA = [ObjAuth getCurrentAuth];
    if (![self.txtOldPass.text isEqualToString:objA.password]) {
        [self textValidateAlertShow:@"Your old password is wrong."];
        return;
    }
    
    if (![self.txtNewPass.text isEqualToString:self.txtConfirmPass.text]) {
        [self textValidateAlertShow:@"Does not match password and confrim password."];
        return;
    }
    [self syncNewPasswordWithPass:self.txtNewPass.text];
    
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)syncNewPasswordWithPass:(NSString *)strPass{
    [SVProgressHUD show];
    ObjAuth * obj = [ObjAuth getCurrentAuth];
    NSDictionary * param =  @{@"new_password":strPass};
    NSLog(@"token %@, token type %@",obj.access_token,obj.token_type);
    [[CoAssetAPIClient sharedClientWithHeader:obj andLink:@""] POST:[NSString stringWithFormat:@"%@",CHANGE_PASS_LINK] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"sync pass successfully return!!! %@",json);
       // NSDictionary * myDict = (NSDictionary *)json;
        [self textValidateAlertShow:@"Password successfully changed."];
        [SVProgressHUD dismiss];
        //[self reloadTheViews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error pass ch %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}


//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:self.viewTextBg];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    [self scrollToOriginal];
}

- (void) scrollToOriginal{
    NSLog(@"scrollView.contentSize height %f",scrollView.contentSize.height);
    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard shown");
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeField.superview setFrame:bkgndRect];
    //[self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    //    self.scrollView.contentInset = contentInsets;
    //    self.scrollView.scrollIndicatorInsets = contentInsets;
    //[self.scrollView setContentOffset:CGPointZero animated:YES];
}

@end
