//
//  ForgetPasswordViewController.m
//  Co Asset
//
//  Created by ark on 9/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "UIColor+expanded.h"

@interface ForgetPasswordViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;


@end

@implementation ForgetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self textFieldSetup];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [self navbarSetup];
}

- (void) navbarSetup{
    [[self navigationController] setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor colorWithHexString:@"ea4c46"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"ea4c46"];
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    lblTitle.text = @"CHANGE PASSWORD";
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = lblTitle;
}

- (void) setupView
{
    self.viewBackground.layer.cornerRadius = 5;
    self.btnUpdate.layer.cornerRadius = 5;
}

- (void) textFieldSetup{
    
    self.txtOldPassword.delegate = self;
    self.txtNewPassword.delegate = self;
    self.txtConfirmPassword.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtOldPassword) {
        [self.txtOldPassword resignFirstResponder];
        [self.txtNewPassword becomeFirstResponder];
    }else if (textField == self.txtNewPassword) {
        [self.txtNewPassword resignFirstResponder];
        [self.txtConfirmPassword becomeFirstResponder];
    }else if (textField == self.txtConfirmPassword) {
        [self.txtConfirmPassword resignFirstResponder];
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
