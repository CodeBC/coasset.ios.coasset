//
//  NewsCollectionViewCell.h
//  Co Asset
//
//  Created by ark on 10/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjNews.h"
@interface NewsCollectionViewCell : UICollectionViewCell

- (void) setupCell;
- (void) loadCell;
- (void) loadCellWithNews:(ObjNews *)obj;
@end
