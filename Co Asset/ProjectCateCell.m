//
//  ProjectCateCell.m
//  Co Asset
//
//  Created by Zayar on 6/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProjectCateCell.h"
#import "StringTable.h"
@interface ProjectCateCell()
@property (strong,nonatomic) UIImageView * imgBgView;
@property (strong,nonatomic) UIView * darkLayerView;
@property (strong,nonatomic) UILabel * lblTitle;
@property (strong,nonatomic) UILabel * lblDescription;
@end

@implementation ProjectCateCell
- (UIImageView *)imgBgView{
    if (!_imgBgView) {
        _imgBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, PROJECT_CATE_CELL_WIDTH, PROJECT_CATE_CELL_HEIGHT)];
        //496 × 391
    }
    return _imgBgView;
}

- (UIView *) darkLayerView{
    if (!_darkLayerView) {
        _darkLayerView = [[UIView alloc] initWithFrame:CGRectMake(0, PROJECT_CATE_CELL_HEIGHT-37, PROJECT_CATE_CELL_WIDTH, 37)];
        [_darkLayerView setBackgroundColor:[UIColor darkTextColor]];
        [_darkLayerView setAlpha:0.4];
    }
    return _darkLayerView;
}

- (UILabel *) lblTitle{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, PROJECT_CATE_CELL_HEIGHT-37, PROJECT_CATE_CELL_WIDTH-20, 37)];
        [_lblTitle setBackgroundColor:[UIColor clearColor]];
        _lblTitle.textColor = [UIColor whiteColor];
        _lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    
    }
    return _lblTitle;
}

- (void)setupCell{
    [self addSubview:self.imgBgView];
    [self addSubview:self.darkLayerView];
    [self addSubview:self.lblTitle];
}

- (void) loadCellWithObjCate:(ObjProjectCate *)objCate{
    [self.imgBgView setImage:[UIImage imageNamed:objCate.strImageName]];
    [self.lblTitle setText:objCate.strName];
}


@end
