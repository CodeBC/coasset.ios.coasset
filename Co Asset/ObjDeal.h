//
//  ObjDeal.h
//  Co Asset
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deal.h"
#import "Project.h"
@interface ObjDeal : NSObject
@property (nonatomic, strong) Deal * deal;
@property (nonatomic, strong) Project * project;
+ (ObjDeal *) setDealSaveAndUpdateFromDic:(NSDictionary *)dic;
+ (ObjDeal *) setDealDetailSaveAndUpdateFromDic:(NSDictionary *)dic;
+ (NSMutableArray *) getDealArray;
+ (void) deleteDeals;
+ (NSArray *) parseDealsWithArray:(NSArray *)arr;
@end
