//
//  MultiLineLabelView.m
//  Co Asset
//
//  Created by Zayar on 5/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MultiLineLabelView.h"
@interface MultiLineLabelView()
@property (nonatomic,strong) UILabel * lblLine1;
@property (nonatomic,strong) UILabel * lblLine2;
@property (nonatomic,strong) UILabel * lblLine3;
@property (nonatomic,strong) UILabel * lblLine4;
@end
@implementation MultiLineLabelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (UILabel *) lblLine1{
    if (!_lblLine1) {
        _lblLine1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 20, 20)];
        _lblLine1.backgroundColor = [UIColor clearColor];
        _lblLine1.textColor = [UIColor blackColor];
        _lblLine1.font = [UIFont systemFontOfSize:14];
        _lblLine1.numberOfLines = 0;
    }
    return _lblLine1;
}

- (UILabel *) lblLine2{
    if (!_lblLine2) {
        _lblLine2 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblLine1.frame.origin.y+self.lblLine1.frame.size.height+3, self.frame.size.width - 20, 20)];
        _lblLine2.backgroundColor = [UIColor clearColor];
        _lblLine2.textColor = [UIColor blackColor];
        _lblLine2.font = [UIFont systemFontOfSize:14];
        _lblLine2.numberOfLines = 0;
    }
    return _lblLine2;
}

- (UILabel *) lblLine3{
    if (!_lblLine3) {
        _lblLine3 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblLine2.frame.origin.y+self.lblLine2.frame.size.height+3, self.frame.size.width - 20, 20)];
        _lblLine3.backgroundColor = [UIColor clearColor];
        _lblLine3.textColor = [UIColor blackColor];
        _lblLine3.font = [UIFont systemFontOfSize:14];
        _lblLine3.numberOfLines = 0;
    }
    return _lblLine3;
}

- (UILabel *) lblLine4{
    if (!_lblLine4) {
        _lblLine4 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblLine3.frame.origin.y+self.lblLine3.frame.size.height+3, self.frame.size.width - 20, 20)];
        _lblLine4.backgroundColor = [UIColor clearColor];
        _lblLine4.textColor = [UIColor blackColor];
        _lblLine4.font = [UIFont systemFontOfSize:14];
        _lblLine4.numberOfLines = 0;
    }
    return _lblLine4;
}

- (void)setUpViews{
    [self addSubview:self.lblLine1];
    [self addSubview:self.lblLine2];
    [self addSubview:self.lblLine3];
    [self addSubview:self.lblLine4];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) setLine1Name:(NSString *)strCapName andName:(NSString *)strName{
    self.lblLine1.text = [NSString stringWithFormat:@"%@ : %@",strCapName,strName];
}

- (void) setLine2Name:(NSString *)strCapName andName:(NSString *)strName andCurrencyCode:(NSString *)strCurrencyCode{
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [_currencyFormatter setCurrencyCode:strCurrencyCode];
    self.lblLine2.text = [NSString stringWithFormat:@"%@ : %@",strCapName,[_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[strName doubleValue]]]];
}

- (void) setLine3Name:(NSString *)strCapName andName:(NSString *)strName{
    self.lblLine3.text = [NSString stringWithFormat:@"%@ : %@",strCapName,strName];
}

- (void) setLine4Name:(NSString *)strCapName andName:(NSString *)strName{
    self.lblLine4.text = [NSString stringWithFormat:@"%@ : %@ %%",strCapName,strName];
}

@end
