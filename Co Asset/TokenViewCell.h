//
//  TokenViewCell.h
//  Co Asset
//
//  Created by Zayar on 6/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjToken.h"
@interface TokenViewCell : UICollectionViewCell
- (void) setUpViews;
- (void)viewLoadWith:(ObjToken *)objToken;
@end
