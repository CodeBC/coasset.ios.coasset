//
//  LabelSingleView.m
//  Co Asset
//
//  Created by Zayar on 5/16/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LabelSingleView.h"
@interface LabelSingleView()
@property (nonatomic,strong) UILabel * lblCap;
@end
@implementation LabelSingleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (UILabel *) lblCap{
    if (!_lblCap) {
        _lblCap = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 20, self.frame.size.height - 10)];
        _lblCap.backgroundColor = [UIColor clearColor];
        _lblCap.textColor = [UIColor blackColor];
        _lblCap.font = [UIFont systemFontOfSize:14];
        _lblCap.numberOfLines = 0;
        _lblCap.textAlignment = NSTextAlignmentCenter;
    }
    return _lblCap;
}

- (void)setUpViews{
    [self addSubview:self.lblCap];
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void) loadWithCapName:(NSString *)strCapName{
    self.lblCap.text = [NSString stringWithFormat:@"%@",strCapName];
}

@end
