//
//  DealCateTableViewCell.m
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DealCateTableViewCell.h"
#import "StringTable.h"
@interface DealCateTableViewCell()
@property (nonatomic,strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIView * titleBgView;
@property (nonatomic, strong) UILabel * lblName;
@property (nonatomic, strong) UIButton * btnSearch;
@end
@implementation DealCateTableViewCell
- (UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, DEAL_CATE_CELL_HEIGHT)];
        _bgImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _bgImageView;
}

- (UIView *) titleBgView{
    if (!_titleBgView) {
        _titleBgView = [[UIView alloc] initWithFrame:CGRectMake(0, DEAL_CATE_CELL_HEIGHT-37, 320, 37)];
        [_titleBgView setBackgroundColor:[UIColor darkTextColor]];
        [_titleBgView setAlpha:0.4];
    }
    return _titleBgView;
}

- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, DEAL_CATE_CELL_HEIGHT-37, 320-20, 37)];
        [_lblName setBackgroundColor:[UIColor clearColor]];
        _lblName.textColor = [UIColor whiteColor];
    }
    return _lblName;
}

- (void)setUpViews{
    [self addSubview:self.bgImageView];
    [self addSubview:self.titleBgView];
    [self addSubview:self.lblName];
}

- (void)viewLoadWithDealCate:(ObjDealCate *)obj{
    [self.bgImageView setImage:[UIImage imageNamed:obj.strImageName]];
    [self.lblName setText:obj.strName];
}
@end
