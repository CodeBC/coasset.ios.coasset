//
//  ObjProject.m
//  Co Asset
//
//  Created by Zayar on 5/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ObjProject.h"
#import "Utility.h"
@implementation ObjProject
@synthesize project;
+ (ObjProject*)sharedClient{
    static ObjProject *_sharedClient = nil;
    
    _sharedClient = [[ObjProject alloc] init];
    
    if(_sharedClient) {
        //_sharedClient = [[DBManager sharedClient] getConfig];
    }
    return _sharedClient;
}

- (id) init{
	if( self = [super init] ){
		project = [Project MR_createEntity];
	}
	
	return self;
}


+ (ObjProject *) setProjectSaveAndUpdateFromDic:(NSDictionary *)dic andDealId:(NSString *)dealId{
    ObjProject * objProject = nil;
    if ([dic count]) {
        objProject = [[ObjProject alloc] init];
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * project_type = [dic objectForKey:@"project_type"];
        NSString * name = [dic objectForKey:@"name"];
        NSInteger developer = [[dic objectForKey:@"developer"] integerValue];
        NSString * photo = [dic objectForKey:@"photo"];
        NSLog(@"photo %@",photo);
        NSString * address_1 = [dic objectForKey:@"address_1"];
        NSString * address_2 = [dic objectForKey:@"address_1"];
        NSString * state_region = [dic objectForKey:@"state_region"];
        NSString * city = [dic objectForKey:@"city"];
        NSString * country = [dic objectForKey:@"country"];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strUniId];
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:project_type shouldCleanWhiteSpace:YES]) project_type = @"";
        if ([Utility stringIsEmpty:name shouldCleanWhiteSpace:YES]) name = @"";
        if ([Utility stringIsEmpty:photo shouldCleanWhiteSpace:YES]) photo = @"";
        if ([Utility stringIsEmpty:address_1 shouldCleanWhiteSpace:YES]) address_1 = @"";
        if ([Utility stringIsEmpty:address_2 shouldCleanWhiteSpace:YES]) address_2 = @"";
        if ([Utility stringIsEmpty:state_region shouldCleanWhiteSpace:YES]) state_region = @"";
        if ([Utility stringIsEmpty:city shouldCleanWhiteSpace:YES]) city = @"";
        if ([Utility stringIsEmpty:country shouldCleanWhiteSpace:YES]) country = @"";
        
        Project * obj = [Project MR_findFirstWithPredicate:predicate inContext:localContext];
        if (obj!= nil) {
            obj.idx = strUniId;
            obj.project_type = project_type;
            obj.deal_id = dealId;
            obj.name = name;
            obj.photo = photo;
            obj.developer = [NSNumber numberWithInteger:developer];
            obj.address_1 = address_1;
            obj.address_2 = address_2;
            obj.state_region = state_region;
            obj.city = city;
            obj.country = country;
            [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            }];
            objProject.project = obj;
        }
        else{
            Project * obj2 = [Project MR_createEntity];
            obj2.idx = strUniId;
            obj2.project_type = project_type;
            obj2.deal_id = dealId;
            obj2.name = name;
            obj2.photo = photo;
            obj2.developer = [NSNumber numberWithInteger:developer];
            obj2.address_1 = address_1;
            obj2.address_2 = address_2;
            obj2.state_region = state_region;
            obj2.city = city;
            obj2.country = country;
            
            [obj2 MR_inContext:localContext];
            
            // Save the modification in the local context
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"Ongoing Project saved");
            }];
            objProject.project = obj2;
        }
    }
    return objProject;
}

+ (Project *) parseProject:(NSDictionary *)dic andDealId:(NSString *)dealId{
    Project * obj;
    if ([dic count]) {
        NSString * strUniId=@"";
        NSInteger idx = [[dic  objectForKey:@"id"] integerValue];
        strUniId =[NSString stringWithFormat:@"%d",idx];
        NSString * project_type = [dic objectForKey:@"project_type"];
        NSString * name = [dic objectForKey:@"name"];
        NSInteger developer = [[dic objectForKey:@"developer"] integerValue];
        NSString * photo = [dic objectForKey:@"photo"];
        NSLog(@"photo %@",photo);
        NSString * address_1 = [dic objectForKey:@"address_1"];
        NSString * address_2 = [dic objectForKey:@"address_1"];
        NSString * state_region = [dic objectForKey:@"state_region"];
        NSString * city = [dic objectForKey:@"city"];
        NSString * country = [dic objectForKey:@"country"];
        
        
        
        if ([Utility stringIsEmpty:strUniId shouldCleanWhiteSpace:YES]) strUniId = @"";
        if ([Utility stringIsEmpty:project_type shouldCleanWhiteSpace:YES]) project_type = @"";
        if ([Utility stringIsEmpty:name shouldCleanWhiteSpace:YES]) name = @"";
        if ([Utility stringIsEmpty:photo shouldCleanWhiteSpace:YES]) photo = @"";
        if ([Utility stringIsEmpty:address_1 shouldCleanWhiteSpace:YES]) address_1 = @"";
        if ([Utility stringIsEmpty:address_2 shouldCleanWhiteSpace:YES]) address_2 = @"";
        if ([Utility stringIsEmpty:state_region shouldCleanWhiteSpace:YES]) state_region = @"";
        if ([Utility stringIsEmpty:city shouldCleanWhiteSpace:YES]) city = @"";
        if ([Utility stringIsEmpty:country shouldCleanWhiteSpace:YES]) country = @"";
        
        obj = [Project MR_createEntity];
            obj.idx = strUniId;
            obj.project_type = project_type;
            obj.deal_id = dealId;
            obj.name = name;
            obj.photo = photo;
            obj.developer = [NSNumber numberWithInteger:developer];
            obj.address_1 = address_1;
            obj.address_2 = address_2;
            obj.state_region = state_region;
            obj.city = city;
            obj.country = country;
    }
    return obj;
}

+ (NSArray *) setProjectForListSaveAndUpdateFromDic:(NSArray *)arr{
    NSMutableArray * arrProject;
    if ([arr count]) {
        arrProject = [[NSMutableArray alloc] init];
    }
    for (NSDictionary * dic in arr) {
        
        ObjProject * objProject;
        if ([dic count]) {
            objProject = [[ObjProject alloc] init];
            NSString * strUniId=@"";
            NSInteger idx = [[dic  objectForKey:@"project_id"] integerValue];
            strUniId =[NSString stringWithFormat:@"%d",idx];
            NSString * name = [dic objectForKey:@"project_name"];
            
            NSString * image_url = [dic objectForKey:@"image_url"];
            NSString * short_description = [dic objectForKey:@"short_description"];

            
            if ([Utility stringIsEmpty:name shouldCleanWhiteSpace:YES]) name = @"";
            if ([Utility stringIsEmpty:image_url shouldCleanWhiteSpace:YES]) image_url = @"";
            if ([Utility stringIsEmpty:name shouldCleanWhiteSpace:YES]) name = @"";
            if ([Utility stringIsEmpty:short_description shouldCleanWhiteSpace:YES]) short_description = @"";
            
            objProject.project.name = name;
            objProject.project.idx = strUniId;
            objProject.project.short_description = short_description;
            objProject.project.photo = image_url;
        }
        [arrProject addObject:objProject];
    }
    
    
    return (NSArray *)arrProject;
}
@end
