//
//  ObjProjectCate.h
//  Co Asset
//
//  Created by Zayar on 5/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjProjectCate : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strDescription;
@property (nonatomic, strong) NSString * strImageName;
@end
