//
//  Project.h
//  Co Asset
//
//  Created by Zayar on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Project : NSManagedObject

@property (nonatomic, retain) NSString * address_1;
@property (nonatomic, retain) NSString * address_2;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * deal_id;
@property (nonatomic, retain) NSNumber * developer;
@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * project_type;
@property (nonatomic, retain) NSString * state_region;
@property (nonatomic, retain) NSString * short_description;

@end
