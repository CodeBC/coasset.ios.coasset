//
//  DealCateTableViewCell.h
//  Co Asset
//
//  Created by Zayar on 5/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDeal.h"
@interface DealOngoingTableViewCell : UITableViewCell
- (void)setUpViews;
- (void)viewLoadWithDealOngoing:(ObjDeal *)obj;
@end
