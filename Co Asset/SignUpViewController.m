//
//  SignUpViewController.m
//  Co Asset
//
//  Created by ark on 9/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SignUpViewController.h"
#import "UIColor+expanded.h"
#import "Utility.h"
#import "StringTable.h"
#import "CoAssetAPIClient.h"
#import "SideMenuViewController.h"

@interface SignUpViewController ()
{
    UITextField *activeField;
}

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txtCountryPrefix;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAgain;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation SignUpViewController
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollVie
CGSize scrollViewOriginalSize;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self textFieldSetup];
    
    scrollViewOriginalSize = self.scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
}

- (void) viewWillAppear:(BOOL)animated{
    [self navbarSetup];
    [self registerForKeyboardNotifications];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self deregisterFromKeyboardNotifications];
}

- (void) setupView{
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    self.btnSignUp.layer.cornerRadius = 5;

}

- (void) navbarSetup{
    [[self navigationController] setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor colorWithHexString:@"ea4c46"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"ea4c46"];
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    lblTitle.text = @"SIGN UP";
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = lblTitle;
}


- (void) textFieldSetup{
    
    self.scrollView.showsVerticalScrollIndicator = YES;
    self.txtFullName.delegate = self;
    self.txtPassword.delegate = self;
    self.txtEmail.delegate = self;
    self.txtEmailAgain.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (IBAction)onSignUP:(id)sender {
    [self validateTheFields];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFullName) {
        [self.txtFullName resignFirstResponder];
        [self.txtFirstName becomeFirstResponder];
    }else if (textField == self.txtFirstName){
        [self.txtFirstName resignFirstResponder];
        [self.txtLastName becomeFirstResponder];
    }
    else if (textField == self.txtLastName){
        [self.txtLastName resignFirstResponder];
        [self.txtPhoneNo becomeFirstResponder];
    }
    else if (textField == self.txtPhoneNo){
        [self.txtPhoneNo resignFirstResponder];
        [self.txtCountryPrefix becomeFirstResponder];
    }
    else if (textField == self.txtCountryPrefix){
        [self.txtCountryPrefix resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    }
    else if (textField == self.txtEmail){
        [self.txtEmail resignFirstResponder];
        [self.txtEmailAgain becomeFirstResponder];
    }
    else if (textField == self.txtEmailAgain) {
        [self.txtEmailAgain resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    }else if (textField == self.txtPassword){
        [self.txtPassword resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   [self moveScrollView:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Active Field is %@",activeField);
    [activeField resignFirstResponder];
    activeField = nil;
    
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard shown");
   
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    //[activeField.superview setFrame:bkgndRect];
    //[self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
    
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    self.scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [self.scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)validateTheFields{
    if ([Utility stringIsEmpty:self.txtFullName.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your name."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtFullName];
        return;
    }
    
    if ([Utility stringIsEmpty:self.txtFirstName.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your first name."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtFirstName];
        return;
    }
    
    if ([Utility stringIsEmpty:self.txtLastName.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your last name."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtLastName];
        return;
    }
    
    if ([Utility stringIsEmpty:self.txtPhoneNo.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your phone no."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtPhoneNo];
        return;
    }
    if ([Utility stringIsEmpty:self.txtCountryPrefix.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your country prefix."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtCountryPrefix];
        return;
    }
    if ([Utility stringIsEmpty:self.txtEmail.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Please enter your email."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtEmail];
        return;
    }
    
    if (![self.txtEmail.text isEqualToString:self.txtEmailAgain.text]) {
        [Utility showAlert:APP_TITLE message:@"Email does not match with retype email!"];
        [self textFieldDidEndEditing:self.txtEmailAgain];
        return;
    }
    
    if ([Utility stringIsEmpty:self.txtPassword.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:@"Password can not be blank."];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:self.txtPassword];
        return;
    }
    [self syncProfile];
}

- (void) syncProfile{
    [SVProgressHUD show];
    NSDictionary * param = @{@"username":self.txtFullName.text,@"last_name":self.txtLastName.text,@"first_name":self.txtFirstName.text,@"country_prefix":self.txtCountryPrefix.text,@"email":self.txtEmail.text,@"cell_phone":self.txtPhoneNo.text,@"password":self.txtPassword.text};
    NSLog(@"param %@",param);
    [[CoAssetAPIClient sharedClient] POST:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",SIGN_UP_LINK]] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncProfile successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        NSString * message = [myDict objectForKey:@"success"];
        [Utility showAlert:APP_TITLE message:message];
        [self.navigationController popViewControllerAnimated:YES];
        [SVProgressHUD dismiss];
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
