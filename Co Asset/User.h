//
//  User.h
//  Co Asset
//
//  Created by Zayar on 5/28/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Account_Type, Profile, Tokens;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) Account_Type *account_type;
@property (nonatomic, retain) Profile *profile;
@property (nonatomic, retain) Tokens *tokens;

@end
