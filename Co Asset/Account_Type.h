//
//  Account_Type.h
//  Co Asset
//
//  Created by Zayar on 5/23/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Account_Type : NSManagedObject

@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * account_type;
@property (nonatomic, retain) NSString * account_expiry;

@end
